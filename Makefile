include .env
export

fmt_cmd = ruff format
lint_cmd = ruff check
dc = docker compose

lint-local:
	$(fmt_cmd) app --check --no-cache
	$(lint_cmd) app --no-cache

fix-local:
	$(fmt_cmd) app --no-cache
	$(fmt_cmd) tests --no-cache
	$(lint_cmd) app --fix --no-cache
	$(lint_cmd) tests --fix --no-cache

up-local:
	python -m uvicorn app.main:app --host 0.0.0.0 --port 8000 --reload

build-app:
	$(dc) build

build-test:
	$(dc) -f compose.test.yaml build

build-all: build-app build-test

up-app:
	$(dc) up -d

down-app:
	$(dc) down

migrate-state:
	$(dc) exec app alembic current

migrate-check:
	$(dc) exec app alembic check

migrate-gen:
	$(dc) exec app alembic revision --autogenerate -m "$(desc)"

migrate-run:
	$(dc) exec app alembic upgrade head

run-tests:
	$(dc) -f compose.test.yaml up --abort-on-container-exit --exit-code-from app-test
	$(dc) -f compose.test.yaml down -v

run-locust:
	locust -f tests/performance/locustfile.py --users 1000 --spawn-rate 100 --run-time 1m --headless --host=http://localhost:8000

logs:
	$(dc) logs -f

shell:
	$(dc) exec app bash

db-shell:
	$(dc) exec db psql -h localhost -U $(POSTGRES_USER) -d $(POSTGRES_DB)

purge-app:
	$(dc) down -v --rmi all

purge-test:
	$(dc) -f compose.test.yaml down -v --rmi all

purge-all: purge-app purge-test
