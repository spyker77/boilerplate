# syntax=docker/dockerfile:1

#####################
# 1 STAGE - BUILDER #
#####################

FROM registry.gitlab.com/spyker77/boilerplate:base AS builder

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1

WORKDIR /code

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential libpq-dev && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir poetry==1.8.3

COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false && \
    poetry install --without dev --no-root --no-interaction --no-ansi


###################
# 2 STAGE - FINAL #
###################

FROM python:3.12.4-slim

WORKDIR /code

COPY --from=builder /usr/local/lib /usr/local/lib
COPY --from=builder /usr/local/bin /usr/local/bin

COPY . ./

RUN chmod +x ./scripts

ARG VERSION=unknown

RUN echo "__version__ = \"$VERSION\"" > app/__init__.py

USER 1000:1000

CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000"]
