#!/bin/bash

# Wait for the database to be ready
./scripts/wait-for-it.sh $DB_HOST:$POSTGRES_PORT -s -t 60 --strict -- &&

    # Run migrations
    alembic upgrade head &&

    # Run application
    uvicorn app.main:app --proxy-headers --host 0.0.0.0 --port 8000
