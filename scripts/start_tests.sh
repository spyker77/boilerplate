#!/bin/bash

# Wait for the database to be ready
./scripts/wait-for-it.sh $DB_HOST:$POSTGRES_PORT -s -t 60 --strict -- &&

    # Run migrations
    alembic upgrade head &&

    # Run pytest
    pytest --cov-report=xml:./output/coverage.xml --cov-report=term-missing --cov=app -p no:cacheprovider --junitxml=./output/report.xml ./tests
