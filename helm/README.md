# Running with Helm

Make sure you have enough resources on the machine before deploying the whole suite of services!

Recommended minimum:

- CPU: 2 cores
- Memory: 16 GB RAM
- Storage: 20 GB available disk space

The Elastic Stack (Elasticsearch, Kibana) and monitoring tools (Prometheus, Grafana) are the most resource-intensive components. If you're running on a machine with limited resources, consider deploying these components selectively or reducing their resource allocations.

## Main Application

First, install the Traefik in your K8s cluster:

   ```bash
   helm repo add traefik https://traefik.github.io/charts && helm repo update
   helm upgrade --install traefik traefik/traefik --namespace traefik --create-namespace
   ```

Second, install the Metrics Server like this:

   ```bash
   helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/ && helm repo update
   helm upgrade --install metrics-server metrics-server/metrics-server \
     --namespace kube-system \
     --set args="{--kubelet-insecure-tls}" \
     --set apiService.create=true
   ```

Finally, run the application with other services:

   ```bash
   helm upgrade --install boilerplate ./helm -f ./helm/values-dev.yaml \
     --namespace dev \
     --create-namespace \
     --set app.image.repository=registry.gitlab.com/spyker77/boilerplate \
     --set app.image.tag=latest \
     --set config.sentryDsn= \
     --set config.secretKey=localIBIpTaSiaU-O5kZzpiOr7B4DaKmsQIo3nlocal \
     --set postgresql.postgresUser=db_user \
     --set postgresql.postgresPassword=db_password \
     --set postgresql.postgresDb=boilerplate \
     --set postgresql.postgresHost=boilerplate-pgbouncer.dev.svc.cluster.local \
     --set pgbouncer.dbHost=boilerplate-postgresql.dev.svc.cluster.local \
     --set redis.redisHost=boilerplate-redis.dev.svc.cluster.local \
     --set rabbitmq.rabbitmqHost=boilerplate-rabbitmq.dev.svc.cluster.local \
     --set traefik.ingressRoute.host=127.0.0.1.nip.io
   ```

This command will:

- Create or update a release named boilerplate.
- Use the Helm chart in the ./helm directory.
- Apply values from ./helm/values-dev.yaml.
- Create the dev namespace if it doesn't exist.
- Set various configuration parameters.

Once deployed, you can access the Swagger UI at <http://127.0.0.1.nip.io/docs>

To view the status of your deployment:

   ```bash
   kubectl get pods -n dev
   ```

To view logs from a specific pod:

   ```bash
   kubectl logs -n dev <pod-name>
   ```

To clean up resources after testing:

   ```bash
   helm uninstall boilerplate -n dev
   kubectl delete namespace dev
   ```

## Logging (Optional)

You can install the Elastic Stack the following way:

   ```bash
   helm repo add elastic https://helm.elastic.co && helm repo update
   helm upgrade --install elastic-operator elastic/eck-operator --namespace elastic-system --create-namespace
   ```

   ```bash
   helm upgrade --install elastic-stack elastic/eck-stack \
     --namespace logging \
     --create-namespace \
     --values https://raw.githubusercontent.com/elastic/cloud-on-k8s/2.13/deploy/eck-stack/examples/agent/fleet-agents.yaml \
     --values ./helm/values-elastic-stack.yaml
   ```

To get password for the `elastic` user in Kibana:

   ```bash
   kubectl get secret --namespace logging elasticsearch-es-elastic-user -o jsonpath='{.data.elastic}' | base64 --decode; echo
   ```

To make Kibana accessible at <https://localhost:5601> in a web browser:

   ```bash
   kubectl port-forward --namespace logging service/kibana-kb-http 5601
   ```

## Monitoring (Optional)

You can also install Prometheus and Grafana like this:

   ```bash
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && helm repo update
   helm upgrade --install prometheus prometheus-community/kube-prometheus-stack \
     --namespace monitoring \
     --create-namespace \
     --set prometheus-node-exporter.hostRootFsMount.enabled=false
   ```

To get password for the `admin` user in Grafana:

   ```bash
   kubectl get secret --namespace monitoring prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
   ```

To make Grafana accessible at <http://localhost:3000> in a web browser:

   ```bash
   kubectl port-forward --namespace monitoring service/prometheus-grafana 3000:80
   ```

To make Prometheus accessible at <http://localhost:9090> in a web browser:

   ```bash
   kubectl port-forward --namespace monitoring service/prometheus-operated 9090
   ```

To collect metrics from Redis, RabbitMQ, PostgreSQL, and PgBouncer install the following exporters.

- PgBouncer Exporter:

   ```bash
   helm upgrade --install pgbouncer-exporter prometheus-community/prometheus-pgbouncer-exporter \
     --namespace monitoring \
     --set config.datasource.user=db_user \
     --set config.datasource.password=db_password \
     --set config.datasource.database=pgbouncer \
     --set config.datasource.host=boilerplate-pgbouncer.dev.svc.cluster.local \
     --set serviceMonitor.enabled=true \
     --set serviceMonitor.namespace=monitoring \
     --set serviceMonitor.interval=15s \
     --set serviceMonitor.labels.release=prometheus \
     --set postgresql.enabled=false
   ```

- Postgres Exporter:

   ```bash
   helm upgrade --install postgres-exporter prometheus-community/prometheus-postgres-exporter \
     --namespace monitoring \
     --set config.datasource.user=db_user \
     --set config.datasource.password=db_password \
     --set config.datasource.database=boilerplate \
     --set config.datasource.host=boilerplate-postgresql.dev.svc.cluster.local \
     --set serviceMonitor.enabled=true \
     --set serviceMonitor.namespace=monitoring \
     --set serviceMonitor.interval=15s \
     --set serviceMonitor.labels.release=prometheus
   ```

- RabbitMQ Exporter:

   ```bash
   helm upgrade --install rabbitmq-exporter prometheus-community/prometheus-rabbitmq-exporter \
     --namespace monitoring \
     --set rabbitmq.url=http://boilerplate-rabbitmq.dev.svc.cluster.local:15672 \
     --set prometheus.monitor.enabled=true \
     --set prometheus.monitor.namespace='{monitoring}' \
     --set prometheus.monitor.interval=15s \
     --set prometheus.monitor.additionalLabels.release=prometheus
   ```

- Redis Exporter:

   ```bash
   helm upgrade --install redis-exporter prometheus-community/prometheus-redis-exporter \
     --namespace monitoring \
     --set redisAddress=redis://boilerplate-redis.dev.svc.cluster.local:6379 \
     --set serviceMonitor.enabled=true \
     --set serviceMonitor.namespace=monitoring \
     --set serviceMonitor.interval=15s \
     --set serviceMonitor.labels.release=prometheus
  ```

Example of custom dashboards for Redis, RabbitMQ, PostgreSQL, and PgBouncer: [763](https://grafana.com/grafana/dashboards/763-redis-dashboard-for-prometheus-redis-exporter-1-x/), [20856](https://grafana.com/grafana/dashboards/20856-rabbitmq-overview-staging-dev-intergration/), [9628](https://grafana.com/grafana/dashboards/13115-postgresql-database-with-kube-prometheus-stack/), and [14022](https://grafana.com/grafana/dashboards/14022-pgbouncer/).

For more information about the project, including local development setup and design decisions, please refer to the main [README.md](../README.md).
