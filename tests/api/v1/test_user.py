from uuid import uuid4

from app.security.rbac import Role


async def test_register_user(test_client):
    user_data = {
        "email": "new@example.com",
        "password": "newpassword",
        "first_name": "New",
        "last_name": "User",
        "role": Role.USER,
    }
    response = await test_client.post("/v1/users/register", json=user_data)
    assert response.status_code == 201
    assert response.json()["email"] == user_data["email"]


async def test_register_existing_user(test_client, test_user):
    user_data = {
        "email": test_user.email,
        "password": "newpassword",
        "first_name": "New",
        "last_name": "User",
        "role": Role.USER,
    }
    response = await test_client.post("/v1/users/register", json=user_data)
    assert response.status_code == 400
    assert "Email already registered" in response.json()["detail"]


async def test_read_me(test_client, test_user, auth_tokens):
    response = await test_client.get("/v1/users/me", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    assert response.status_code == 200
    assert response.json()["email"] == test_user.email


async def test_update_me(test_client, auth_tokens):
    update_data = {"first_name": "Updated"}
    response = await test_client.patch(
        "/v1/users/me",
        json=update_data,
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["first_name"] == "Updated"


async def test_read_user(test_client, test_user, auth_tokens):
    response = await test_client.get(
        f"/v1/users/{test_user.id}",
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["id"] == str(test_user.id)


async def test_read_nonexistent_user(test_client, auth_tokens):
    nonexistent_id = uuid4()
    response = await test_client.get(
        f"/v1/users/{nonexistent_id}",
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 404


async def test_update_user(test_client, admin_user_and_tokens):
    admin_user, admin_tokens = admin_user_and_tokens
    update_data = {"first_name": "AdminUpdated"}
    response = await test_client.patch(
        f"/v1/users/{admin_user.id}",
        json=update_data,
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["first_name"] == "AdminUpdated"


async def test_update_user_unauthorized(test_client, test_user, auth_tokens):
    update_data = {"first_name": "Unauthorized"}
    response = await test_client.patch(
        f"/v1/users/{test_user.id}",
        json=update_data,
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 403


async def test_deactivate_user(test_client, admin_user_and_tokens):
    admin_user, admin_tokens = admin_user_and_tokens
    response = await test_client.patch(
        f"/v1/users/{admin_user.id}/deactivate",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["is_active"] is False


async def test_reactivate_user(test_client, admin_user_and_tokens):
    admin_user, admin_tokens = admin_user_and_tokens
    # First deactivate.
    await test_client.patch(
        f"/v1/users/{admin_user.id}/deactivate",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    # Then reactivate.
    response = await test_client.patch(
        f"/v1/users/{admin_user.id}/reactivate",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["is_active"] is True


async def test_hard_delete_user(test_client, admin_user_and_tokens):
    admin_user, admin_tokens = admin_user_and_tokens
    response = await test_client.delete(
        f"/v1/users/{admin_user.id}",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 204


async def test_hard_delete_nonexistent_user(test_client, admin_user_and_tokens):
    _, admin_tokens = admin_user_and_tokens
    nonexistent_id = uuid4()
    response = await test_client.delete(
        f"/v1/users/{nonexistent_id}",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 404
