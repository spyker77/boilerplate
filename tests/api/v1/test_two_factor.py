import base64

import pyotp


async def test_enable_2fa(test_client, test_user, user_service, auth_tokens, db_session):
    response = await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})

    assert response.status_code == 200
    data = response.json()
    assert "secret" in data
    assert "qr_code" in data

    # Verify that the qr_code is a valid base64 encoded string.
    base64.b64decode(data["qr_code"])

    # Get the latest data from database.
    await db_session.refresh(test_user)

    # Verify that 2FA is now enabled for the user.
    updated_user = await user_service.get_user_by_id(test_user.id)
    assert updated_user.two_factor_enabled
    assert updated_user.two_factor_secret == data["secret"]

    # Try to enable 2FA again.
    response = await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    assert response.status_code == 400
    assert response.json()["detail"] == "2FA is already enabled"


async def test_verify_2fa(test_client, auth_tokens):
    # First, enable 2FA.
    response = await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    secret = response.json()["secret"]

    # Generate a valid code.
    totp = pyotp.TOTP(secret)
    valid_code = totp.now()

    response = await test_client.post(
        "/v1/2fa/verify",
        json={"code": valid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["message"] == "2FA verification successful"

    # Try with an invalid code.
    invalid_code = "000000"
    response = await test_client.post(
        "/v1/2fa/verify",
        json={"code": invalid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid 2FA code"


async def test_complete_2fa(test_client, auth_tokens):
    # First, enable 2FA.
    response = await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    secret = response.json()["secret"]

    # Generate a valid code.
    totp = pyotp.TOTP(secret)
    valid_code = totp.now()

    response = await test_client.post(
        "/v1/2fa/complete",
        json={"code": valid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert "refresh_token" in response.json()

    # Try with an invalid code.
    invalid_code = "000000"
    response = await test_client.post(
        "/v1/2fa/complete",
        json={"code": invalid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid 2FA code"


async def test_disable_2fa(test_client, test_user, user_service, auth_tokens):
    # First, enable 2FA.
    response = await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    secret = response.json()["secret"]

    # Generate a valid code
    totp = pyotp.TOTP(secret)
    valid_code = totp.now()

    response = await test_client.post(
        "/v1/2fa/disable",
        json={"password": "testpassword", "code": valid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["message"] == "2FA has been disabled"

    # Verify that 2FA is now disabled for the user.
    updated_user = await user_service.get_user_by_id(test_user.id)
    assert not updated_user.two_factor_enabled
    assert updated_user.two_factor_secret is None

    # Try with an invalid password.
    response = await test_client.post(
        "/v1/2fa/disable",
        json={"password": "wrongpassword", "code": valid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 401

    # Try with an invalid code.
    invalid_code = "000000"
    response = await test_client.post(
        "/v1/2fa/disable",
        json={"password": "testpassword", "code": invalid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid 2FA code"


async def test_generate_backup_codes(test_client, test_user, user_service, auth_tokens, db_session):
    # First, enable 2FA.
    await test_client.post("/v1/2fa/enable", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})

    # Now generate backup codes.
    response = await test_client.post(
        "/v1/2fa/generate-backup-codes",
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 200
    backup_codes = response.json()
    assert len(backup_codes) == 10
    assert all(isinstance(code, str) and len(code) == 8 for code in backup_codes)

    # Get the latest data from database.
    await db_session.refresh(test_user)

    # Verify that backup codes are stored (hashed) in the user model.
    updated_user = await user_service.get_user_by_id(test_user.id)
    assert len(updated_user.backup_codes) == 10

    # Try to generate backup codes when 2FA is not enabled – disable 2FA.
    secret = (await user_service.get_user_by_id(test_user.id)).two_factor_secret
    totp = pyotp.TOTP(secret)
    valid_code = totp.now()
    await test_client.post(
        "/v1/2fa/disable",
        json={"password": "testpassword", "code": valid_code},
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )

    response = await test_client.post(
        "/v1/2fa/generate-backup-codes",
        headers={"Authorization": f"Bearer {auth_tokens.access_token}"},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "2FA is not enabled"
