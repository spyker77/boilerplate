import secrets

import pytest


async def test_unauthorized_access(test_client, auth_tokens):
    response = await test_client.get("/v1/api-keys/", headers={"Authorization": f"Bearer {auth_tokens.access_token}"})
    assert response.status_code == 403


async def test_list_api_keys(test_client, admin_user_and_tokens):
    _, admin_tokens = admin_user_and_tokens
    response = await test_client.get("/v1/api-keys/", headers={"Authorization": f"Bearer {admin_tokens.access_token}"})
    assert response.status_code == 200
    assert isinstance(response.json(), list)


async def test_create_api_key(test_client, admin_user_and_tokens):
    _, admin_tokens = admin_user_and_tokens
    api_key_data = {"name": "Test API Key", "expires_in_days": 30}
    response = await test_client.post(
        "/v1/api-keys/",
        json=api_key_data,
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == 200
    assert response.json()["name"] == "Test API Key"


@pytest.mark.parametrize("key_exists", [True, False])
async def test_get_api_key(test_client, admin_user_and_tokens, api_key_service, key_exists):
    admin_user, admin_tokens = admin_user_and_tokens

    if key_exists:
        api_key = await api_key_service.create_api_key(admin_user.id, "Test API Key", 30)
        key = api_key.key
        expected_status = 200
    else:
        key = secrets.token_urlsafe(32)
        expected_status = 404

    response = await test_client.get(
        f"/v1/api-keys/{key}",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == expected_status
    if key_exists:
        assert response.json()["key"] == key


@pytest.mark.parametrize("key_exists", [True, False])
async def test_rotate_api_key(test_client, admin_user_and_tokens, api_key_service, key_exists):
    admin_user, admin_tokens = admin_user_and_tokens

    if key_exists:
        api_key = await api_key_service.create_api_key(admin_user.id, "Test API Key", 30)
        key = api_key.key
        expected_status = 200
    else:
        key = secrets.token_urlsafe(32)
        expected_status = 404

    response = await test_client.post(
        f"/v1/api-keys/{key}/rotate",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == expected_status
    if key_exists:
        assert response.json()["key"] != key


@pytest.mark.parametrize("key_exists", [True, False])
async def test_deactivate_api_key(test_client, admin_user_and_tokens, api_key_service, key_exists):
    admin_user, admin_tokens = admin_user_and_tokens

    if key_exists:
        api_key = await api_key_service.create_api_key(admin_user.id, "Test API Key", 30)
        key = api_key.key
        expected_status = 200
    else:
        key = secrets.token_urlsafe(32)
        expected_status = 404

    response = await test_client.patch(
        f"/v1/api-keys/{key}/deactivate",
        headers={"Authorization": f"Bearer {admin_tokens.access_token}"},
    )
    assert response.status_code == expected_status
    if key_exists:
        assert response.json()["message"] == "API key deactivated successfully"
