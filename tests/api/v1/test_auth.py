import asyncio
from datetime import timedelta

import pytest

from app.schemas.user import UserUpdate
from app.security.jwt import TokenType, create_token


@pytest.mark.parametrize(
    "credentials, expected_status",
    [
        ({"password": "testpassword"}, 200),
        ({"password": "wrongpassword"}, 401),
    ],
)
async def test_login(test_client, test_user, credentials, expected_status):
    credentials["username"] = test_user.email

    response = await test_client.post("/v1/auth/login", data=credentials)
    assert response.status_code == expected_status
    if expected_status == 200:
        assert "access_token" in response.json()
        assert "refresh_token" in response.json()


@pytest.mark.parametrize(
    "token, expected_status, expected_response",
    [
        ("valid_token", 200, {"detail": "Successfully logged out"}),
        ("invalid_token", 401, {"detail": "Invalid or expired token"}),
    ],
)
async def test_logout(test_client, auth_tokens, token, expected_status, expected_response):
    if token == "valid_token":
        token = auth_tokens.refresh_token

    response = await test_client.post("/v1/auth/logout", json={"refresh_token": token})
    assert response.status_code == expected_status
    assert response.json() == expected_response


@pytest.mark.parametrize(
    "scenario, token_type, expected_status",
    [
        ("valid_token", TokenType.REFRESH, 200),
        ("invalid_token", TokenType.ACCESS, 401),
        ("expired_token", TokenType.REFRESH, 401),
    ],
)
async def test_refresh_token(test_client, test_user, redis_client, scenario, token_type, expected_status):
    match scenario:
        case "valid_token":
            token = await create_token({"sub": test_user.email}, token_type, redis=redis_client)
        case "invalid_token":
            token = "invalid_token"
        case "expired_token":
            # Create a token that expires immediately.
            token = await create_token(
                {"sub": test_user.email},
                token_type,
                redis=redis_client,
                expires_delta=timedelta(seconds=2),
            )
            await asyncio.sleep(2)  # wait for the token to expire

    response = await test_client.post("/v1/auth/refresh", json={"refresh_token": token})
    assert response.status_code == expected_status

    match expected_status:
        case 200:
            assert "access_token" in response.json()
            assert "refresh_token" in response.json()
        case _:
            assert "detail" in response.json()


@pytest.mark.parametrize(
    "scenario, is_verified, expected_status",
    [
        ("unverified_user", False, 200),
        ("verified_user", True, 200),
        ("non_existent_user", None, 200),
    ],
)
async def test_request_verify_token(test_client, test_user, user_service, scenario, is_verified, expected_status):
    if scenario == "non_existent_user":
        email = "nonexistent@example.com"
    else:
        email = test_user.email
        await user_service.update_user(test_user.id, UserUpdate(is_verified=is_verified))

    response = await test_client.post("/v1/auth/request-verify-token", json={"email": email})
    assert response.status_code == expected_status
    assert response.json() == {
        "detail": "If the email is registered and not verified, a verification token has been sent"
    }


@pytest.mark.parametrize(
    "scenario, is_verified, expected_status, expected_message",
    [
        ("valid_token", False, 200, "User successfully verified"),
        ("invalid_token", False, 400, "Invalid or expired token"),
        ("already_verified", True, 400, "User already verified"),
    ],
)
async def test_verify_user(
    test_client,
    test_user,
    user_service,
    redis_client,
    scenario,
    is_verified,
    expected_status,
    expected_message,
):
    await user_service.update_user(test_user.id, UserUpdate(is_verified=is_verified))

    match scenario:
        case "valid_token" | "already_verified":
            token = await create_token({"sub": test_user.email}, TokenType.VERIFY, redis=redis_client)
        case "invalid_token":
            token = "invalid_token"

    response = await test_client.post("/v1/auth/verify", json={"token": token})
    assert response.status_code == expected_status
    assert response.json()["detail"] == expected_message


@pytest.mark.parametrize(
    "scenario, expected_status",
    [
        ("existing_user", 200),
        ("non_existent_user", 200),
    ],
)
async def test_forgot_password(test_client, test_user, scenario, expected_status):
    if scenario == "existing_user":
        email = test_user.email
    else:
        email = "nonexistent@example.com"

    response = await test_client.post("/v1/auth/forgot-password", json={"email": email})
    assert response.status_code == expected_status
    assert response.json() == {"detail": "If the email is registered, a password reset token has been sent"}


@pytest.mark.parametrize(
    "scenario, new_password, expected_status",
    [
        ("valid_token", "newpassword", 200),
        ("invalid_token", "newpassword", 400),
        ("same_password", "testpassword", 400),
    ],
)
async def test_reset_password(test_client, test_user, redis_client, scenario, new_password, expected_status):
    match scenario:
        case "valid_token" | "same_password":
            token = await create_token({"sub": test_user.email}, TokenType.RESET, redis=redis_client)
        case "invalid_token":
            token = "invalid_token"

    response = await test_client.post("/v1/auth/reset-password", json={"token": token, "new_password": new_password})
    assert response.status_code == expected_status

    match expected_status:
        case 200:
            assert response.json() == {"detail": "Password successfully reset"}
        case _:
            assert "detail" in response.json()
