from decimal import Decimal
from unittest.mock import AsyncMock, MagicMock, patch

import httpx
import pytest

from app.config import MISC_CONF
from app.models.currency import Currency
from app.utils.crypto import fetch_binance_ticker_price, get_exchange_rates


async def test_fetch_binance_ticker_price(mock_httpx_get):
    mock_response = MagicMock()
    mock_response.raise_for_status.return_value = None
    mock_response.json.return_value = [{"symbol": "BTCUSDT", "price": "30000.00"}]
    mock_httpx_get.return_value = mock_response

    result = await fetch_binance_ticker_price()
    assert result == [{"symbol": "BTCUSDT", "price": "30000.00"}]
    mock_httpx_get.assert_called_once_with(MISC_CONF.BINANCE_API_URL + "/api/v3/ticker/price")


async def test_fetch_binance_ticker_price_retry(mock_httpx_get):
    mock_error_response = MagicMock()
    mock_error_response.raise_for_status.side_effect = httpx.HTTPStatusError(
        message="", request=AsyncMock(), response=AsyncMock()
    )

    mock_success_response = MagicMock()
    mock_success_response.raise_for_status.return_value = None
    mock_success_response.json.return_value = [{"symbol": "BTCUSDT", "price": "30000.00"}]

    mock_httpx_get.side_effect = [mock_error_response, mock_success_response]

    result = await fetch_binance_ticker_price()
    assert result == [{"symbol": "BTCUSDT", "price": "30000.00"}]
    assert mock_httpx_get.call_count == 2


async def test_get_exchange_rates():
    with patch(
        "app.utils.crypto.fetch_binance_ticker_price",
        return_value=[
            {"symbol": "BTCUSDT", "price": "30000.00"},
            {"symbol": "ETHUSDT", "price": "2000.00"},
            {"symbol": "ETHBTC", "price": "0.06666667"},
        ],
    ):
        result = await get_exchange_rates([Currency(code="BTC"), Currency(code="ETH"), Currency(code="USDT")])

        assert "BTC" in result
        assert "ETH" in result
        assert "USDT" in result

        assert result["BTC"]["USDT"] == Decimal("30000.00")
        assert result["USDT"]["BTC"] == Decimal("1") / Decimal("30000.00")
        assert result["ETH"]["USDT"] == Decimal("2000.00")
        assert result["USDT"]["ETH"] == Decimal("1") / Decimal("2000.00")
        assert result["ETH"]["BTC"] == Decimal("0.06666667")
        assert result["BTC"]["ETH"] == Decimal("1") / Decimal("0.06666667")

        assert result["BTC"]["BTC"] == Decimal("1")
        assert result["ETH"]["ETH"] == Decimal("1")
        assert result["USDT"]["USDT"] == Decimal("1")


@pytest.mark.parametrize(
    "exception, expected_result",
    [
        (httpx.HTTPStatusError(message="", request=AsyncMock(), response=AsyncMock()), {}),
        (Exception("Test error"), {}),
    ],
)
async def test_get_exchange_rates_errors(exception, expected_result):
    with patch("app.utils.crypto.fetch_binance_ticker_price", side_effect=exception):
        result = await get_exchange_rates([Currency(code="BTC"), Currency(code="ETH"), Currency(code="USDT")])
        assert result == expected_result
