from uuid import UUID

import orjson
import pytest

from app.schemas.user import UserUpdate
from app.utils.serializers import json_deserializer, json_serializer, key_deserializer, key_serializer


@pytest.mark.parametrize(
    "input_key, expected",
    [
        ("test_key", b"test_key"),
        (None, None),
        ("", None),
        ("unicode_key_🔑", b"unicode_key_\xf0\x9f\x94\x91"),
    ],
)
def test_key_serializer(input_key, expected):
    assert key_serializer(input_key) == expected


@pytest.mark.parametrize(
    "input_key, expected",
    [
        (b"test_key", "test_key"),
        (None, None),
        (b"", None),
        (b"unicode_key_\xf0\x9f\x94\x91", "unicode_key_🔑"),
    ],
)
def test_key_deserializer(input_key, expected):
    assert key_deserializer(input_key) == expected


def test_json_serializer():
    # Test serializing a dictionary.
    obj_dict = {"test": "data", "value": 123}
    expected_dict = orjson.dumps(obj_dict)
    assert json_serializer(obj_dict) == expected_dict

    # Test serializing a bytes object.
    obj_bytes = b'{"test":"data","value":123}'
    assert json_serializer(obj_bytes) == obj_bytes

    # Test serializing a Pydantic schema.
    obj_schema = UserUpdate(first_name="test")
    expected_schema = obj_schema.model_dump_json().encode("utf-8")
    assert json_serializer(obj_schema) == expected_schema

    # Test serializing a UUID.
    uuid_obj = UUID("123e4567-e89b-12d3-a456-426614174000")
    expected_uuid = orjson.dumps(str(uuid_obj))
    assert json_serializer(uuid_obj) == expected_uuid

    # Test the non-serializable object.
    class NonSerializableObject: ...

    with pytest.raises(TypeError) as excinfo:
        json_serializer(NonSerializableObject())
    assert "Type is not JSON serializable: NonSerializableObject" in str(excinfo.value)


@pytest.mark.parametrize(
    "input_data, expected",
    [
        (orjson.dumps({"test": "data", "value": 123}), {"test": "data", "value": 123}),
        (None, None),
        (b"", None),  # empty bytes should return None
        (b" ", None),  # whitespace should also return None
        (b"null", None),  # JSON null should deserialize to None
        (b"[]", []),  # empty array
        (b"{}", {}),  # empty object
    ],
)
def test_json_deserializer(input_data, expected):
    assert json_deserializer(input_data) == expected


def test_json_deserializer_invalid_json():
    with pytest.raises(orjson.JSONDecodeError):
        json_deserializer(b"{invalid json}")
