import random
import string

from locust import FastHttpUser, constant, task


def random_string(length):
    return "".join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


class UserTasks(FastHttpUser):
    wait_time = constant(0)

    @task(1)
    def register_user(self):
        payload = {
            "email": f"{random_string(8)}@example.com",
            "password": random_string(12),
            "first_name": random_string(8),
            "last_name": random_string(8),
        }

        with self.client.post("/api/v1/auth/register", json=payload, catch_response=True) as response:
            if response.status_code == 201:
                response.success()
            else:
                response.failure(f"Registration failed with status code: {response.status_code}")

    @task(2)
    def get_user_me_unauthorized(self):
        with self.client.get("/api/v1/users/me", catch_response=True) as response:
            if response.status_code == 401:
                response.success()
            else:
                response.failure(f"Unexpected status code: {response.status_code}")

    def on_start(self):
        # You can add any setup logic here, like getting a CSRF token if needed, etc.
        pass
