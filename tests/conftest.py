from unittest.mock import AsyncMock, patch
from uuid import uuid4

import jwt
import pytest
from httpx import ASGITransport, AsyncClient

from app.config import MISC_CONF
from app.db import engine, get_db
from app.main import app
from app.models._base import Base
from app.redis import get_redis
from app.schemas.user import UserCreate
from app.security.auth import create_auth_tokens
from app.security.jwt import ALGORITHM, TokenType, create_token
from app.security.rbac import Role
from app.services.api_key import APIKeyService
from app.services.user import UserService

DEFAULT_USER_ARGS = {
    "email": f"test-{uuid4()}@example.com",
    "password": "testpassword",
    "first_name": "Test",
    "last_name": "User",
    "role": Role.USER,
    "is_active": True,
    "is_verified": False,
    "is_superuser": False,
    "is_merchant": False,
    "two_factor_enabled": False,
    "two_factor_secret": None,
}


@pytest.fixture(autouse=True)
def anyio_backend():
    return "asyncio"


@pytest.fixture(autouse=True)
async def prepare_db():
    # Initially, the starting script is executed and the tables are created,
    # thus only for a subsequent tests we need to drop and recreate the tables.
    yield
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@pytest.fixture
async def db_session():
    async for db in get_db():
        yield db


@pytest.fixture
async def redis_client():
    async for redis in get_redis():
        yield redis
        await redis.flushdb()


@pytest.fixture()
async def test_client():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test/api") as ac:  # type: ignore
        yield ac


@pytest.fixture
async def mock_httpx_get():
    with patch("httpx.AsyncClient.get", new_callable=AsyncMock) as mock:
        yield mock


@pytest.fixture
async def user_service(db_session):
    yield UserService(db_session)


@pytest.fixture
async def api_key_service(db_session, redis_client):
    yield APIKeyService(db_session, redis_client)


@pytest.fixture
async def create_test_user(user_service):
    async def create(**kwargs):
        user_data = DEFAULT_USER_ARGS.copy()
        user_data.update(kwargs)

        user_create = UserCreate(**user_data)
        return await user_service.create_user(user_create)

    return create


@pytest.fixture
async def test_user(create_test_user):
    return await create_test_user()


@pytest.fixture
async def create_test_token(redis_client):
    async def create(token_type=TokenType.ACCESS, sub="test@example.com", audience="boilerplate", **kwargs):
        data = {"sub": sub}
        token = await create_token(data, token_type, redis=redis_client, audience=audience, **kwargs)
        payload = jwt.decode(token, MISC_CONF.SECRET_KEY, algorithms=[ALGORITHM], audience=audience)
        return token, payload

    return create


@pytest.fixture
async def auth_tokens(test_user, redis_client):
    return await create_auth_tokens(test_user, redis_client)


@pytest.fixture
async def admin_user_and_tokens(create_test_user, redis_client):
    admin_user = await create_test_user(role=Role.ADMIN)
    admin_tokens = await create_auth_tokens(admin_user, redis_client)
    return admin_user, admin_tokens
