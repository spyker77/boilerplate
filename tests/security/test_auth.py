from unittest.mock import MagicMock, patch

import pytest
from fastapi import HTTPException

from app.security.auth import (
    authenticate_user,
    create_auth_tokens,
    get_current_user,
    get_user_from_api_key,
    get_user_from_token,
    refresh_auth_tokens,
)
from app.security.jwt import JSONWebToken, TokenType


async def test_authenticate_user_success(test_user, user_service):
    authenticated_user = await authenticate_user(test_user.email, "testpassword", user_service)
    assert authenticated_user.id == test_user.id


async def test_authenticate_user_failure(user_service):
    with pytest.raises(HTTPException) as exc_info:
        await authenticate_user("nonexistent@example.com", "wrongpassword", user_service)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Incorrect username or password"


async def test_create_and_refresh_auth_tokens(test_user, user_service, redis_client):
    initial_tokens = await create_auth_tokens(test_user, redis_client)
    assert isinstance(initial_tokens, JSONWebToken)
    assert initial_tokens.token_type == TokenType.BEARER

    refreshed_tokens = await refresh_auth_tokens(initial_tokens.refresh_token, user_service, redis_client)
    assert isinstance(refreshed_tokens, JSONWebToken)
    assert refreshed_tokens.access_token != initial_tokens.access_token


async def test_refresh_auth_tokens_invalid_token(user_service, redis_client):
    with pytest.raises(HTTPException) as exc_info:
        await refresh_auth_tokens("invalid_token", user_service, redis_client)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Invalid or expired token"


async def test_refresh_auth_tokens_user_not_found(user_service, redis_client):
    with patch("app.security.auth.verify_token", return_value="nonexistent@example.com"):
        with pytest.raises(HTTPException) as exc_info:
            await refresh_auth_tokens("valid_but_nonexistent_user_token", user_service, redis_client)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "User not found"


async def test_get_user_from_token(test_user, user_service, redis_client, auth_tokens):
    token_user = await get_user_from_token(auth_tokens.access_token, user_service, redis_client)
    assert token_user.id == test_user.id


async def test_get_user_from_token_invalid_token(user_service, redis_client):
    with pytest.raises(HTTPException) as exc_info:
        await get_user_from_token("invalid_token", user_service, redis_client)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Invalid or expired token"


async def test_get_user_from_token_user_not_found(user_service, redis_client):
    with patch("app.security.auth.verify_token", return_value="nonexistent@example.com"):
        with pytest.raises(HTTPException) as exc_info:
            await get_user_from_token("valid_but_nonexistent_user_token", user_service, redis_client)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "User not found"


async def test_get_user_from_api_key(test_user, user_service, api_key_service):
    api_key = await api_key_service.create_api_key(test_user.id, "Test Key")
    api_key_user = await get_user_from_api_key(api_key.key, api_key_service, user_service)
    assert api_key_user.id == test_user.id


async def test_get_user_from_api_key_invalid_key(api_key_service, user_service):
    with pytest.raises(HTTPException) as exc_info:
        await get_user_from_api_key("invalid_api_key", api_key_service, user_service)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Invalid API key"


async def test_get_user_from_api_key_user_not_found(api_key_service, user_service):
    with patch.object(api_key_service, "get_api_key", return_value=MagicMock(user_id="nonexistent_id")):
        with patch.object(user_service, "get_user_by_id", return_value=None):
            with pytest.raises(HTTPException) as exc_info:
                await get_user_from_api_key("valid_but_nonexistent_user_api_key", api_key_service, user_service)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "User not found"


async def test_get_current_user_with_api_key(test_user, user_service, api_key_service, redis_client):
    api_key = await api_key_service.create_api_key(test_user.id, "Test Key")
    current_user = await get_current_user(api_key.key, None, user_service, api_key_service, redis_client)
    assert current_user.id == test_user.id


async def test_get_current_user_with_token(test_user, user_service, api_key_service, redis_client, auth_tokens):
    current_user = await get_current_user(None, auth_tokens.access_token, user_service, api_key_service, redis_client)
    assert current_user.id == test_user.id
