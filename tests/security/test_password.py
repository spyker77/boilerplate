import pytest
from argon2.exceptions import InvalidHashError

from app.security.password import get_password_hash, needs_rehash, verify_password


def test_get_password_hash():
    password = "test_password"
    hashed = get_password_hash(password)
    assert hashed.startswith("$argon2")
    assert hashed != password


def test_verify_password_correct():
    password = "correct_password"
    hashed = get_password_hash(password)
    assert verify_password(password, hashed)


def test_verify_password_incorrect():
    password = "correct_password"
    wrong_password = "wrong_password"
    hashed = get_password_hash(password)
    assert not verify_password(wrong_password, hashed)


def test_verify_password_invalid_hash():
    password = "test_password"
    invalid_hash = "invalid_hash"
    assert not verify_password(password, invalid_hash)


def test_needs_rehash():
    password = "test_password"
    hashed = get_password_hash(password)
    assert not needs_rehash(hashed)


def test_needs_rehash_invalid_hash():
    with pytest.raises(InvalidHashError):
        needs_rehash("invalid_hash")


@pytest.mark.parametrize(
    "password",
    [
        "short",
        "medium_password",
        "very_long_password_with_lots_of_characters_123456789",
        "password_with_special_chars!@#$%^&*()",
        "パスワード",  # unicode password
    ],
)
def test_password_hashing_and_verification(password):
    hashed = get_password_hash(password)
    assert verify_password(password, hashed)
    assert not verify_password(password + "wrong", hashed)
