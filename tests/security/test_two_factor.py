import base64
from unittest.mock import patch

import pyotp

from app.schemas.user import UserUpdate
from app.security.two_factor import TwoFactorSetup, generate_2fa_secret, generate_backup_codes, verify_2fa_code


async def test_generate_2fa_secret(test_user, user_service):
    with patch("pyotp.random_base32", return_value="test_secret"), patch("qrcode.make") as mock_qr_make:
        mock_qr_image = mock_qr_make.return_value
        mock_qr_image.save.side_effect = lambda f, format: f.write(b"mocked_qr_code")

        result = await generate_2fa_secret(test_user, user_service)

        assert isinstance(result, TwoFactorSetup)
        assert result.secret == "test_secret"
        assert isinstance(result.qr_code, str)

        # Decode the base64 string and check if it matches the mocked QR code.
        decoded_qr_code = base64.b64decode(result.qr_code.encode("utf-8"))
        assert decoded_qr_code == b"mocked_qr_code"

        updated_user = await user_service.get_user_by_id(test_user.id)
        assert updated_user.two_factor_enabled
        assert updated_user.two_factor_secret == "test_secret"

        # Verify that the qr_code is a valid base64 string.
        base64.b64decode(result.qr_code)


async def test_verify_2fa_code(test_user, user_service):
    # First, enable 2FA for the test user.
    secret = pyotp.random_base32()
    await user_service.update_user(test_user.id, UserUpdate(two_factor_enabled=True, two_factor_secret=secret))

    # Get the updated user
    user_with_2fa = await user_service.get_user_by_id(test_user.id)

    # Generate a valid TOTP code.
    totp = pyotp.TOTP(secret)
    valid_code = totp.now()

    assert verify_2fa_code(user_with_2fa, valid_code)
    assert not verify_2fa_code(user_with_2fa, "invalid_code")

    # Test with user without 2FA.
    assert not verify_2fa_code(test_user, "any_code")

    # Test with `None` user.
    assert not verify_2fa_code(None, "any_code")


async def test_generate_backup_codes(test_user, user_service):
    with patch("secrets.token_hex", return_value="abcd1234"):
        backup_codes = await generate_backup_codes(test_user, user_service)

        assert len(backup_codes) == 10
        assert all(code == "abcd1234" for code in backup_codes)

        updated_user = await user_service.get_user_by_id(test_user.id)
        assert len(updated_user.backup_codes) == 10
        assert all(code != "abcd1234" for code in updated_user.backup_codes)  # codes should be hashed
