from datetime import timedelta

import pytest
from freezegun import freeze_time

from app.security.jwt import TOKEN_PREFIX, TokenType, decode_token, is_token_revoked, revoke_token, verify_token


@pytest.mark.parametrize("token_type", [TokenType.ACCESS, TokenType.REFRESH])
async def test_create_and_verify_token(token_type, redis_client, create_test_token):
    token, payload = await create_test_token(token_type)
    assert payload["sub"] == "test@example.com"
    assert payload["token_type"] == token_type.value
    assert all(key in payload for key in ["exp", "iat", "jti"])

    email = await verify_token(token, token_type, redis=redis_client)
    assert email == "test@example.com"


async def test_create_token_with_custom_expiry(create_test_token):
    custom_expiry = timedelta(hours=2)
    _, payload = await create_test_token(expires_delta=custom_expiry)
    assert payload["exp"] - payload["iat"] == int(custom_expiry.total_seconds())


async def test_create_token_with_redis(redis_client, create_test_token):
    _, payload = await create_test_token()
    assert await redis_client.get(f"{TOKEN_PREFIX}{payload['jti']}") == "valid"


@pytest.mark.parametrize(
    "create_type, verify_type, expected",
    [
        (TokenType.ACCESS, TokenType.REFRESH, None),
        (TokenType.REFRESH, TokenType.ACCESS, None),
        (TokenType.ACCESS, TokenType.ACCESS, "test@example.com"),
    ],
)
async def test_verify_token_types(create_type, verify_type, expected, redis_client, create_test_token):
    token, _ = await create_test_token(create_type)
    email = await verify_token(token, verify_type, redis=redis_client)
    assert email == expected


async def test_verify_token_with_invalid_audience(redis_client, create_test_token):
    token, _ = await create_test_token(audience="custom_audience")
    email = await verify_token(token, TokenType.ACCESS, expected_audience="wrong_audience", redis=redis_client)
    assert email is None


@freeze_time("2024-01-01")
async def test_verify_token_expired(redis_client, create_test_token):
    token, _ = await create_test_token(expires_delta=timedelta(seconds=1))
    with freeze_time("2024-01-02"):
        email = await verify_token(token, TokenType.ACCESS, redis=redis_client)
        assert email is None


async def test_token_lifecycle(redis_client, create_test_token):
    token, payload = await create_test_token()
    jti = payload["jti"]

    # Valid token.
    assert await verify_token(token, TokenType.ACCESS, redis=redis_client) == "test@example.com"
    assert await decode_token(token, redis=redis_client) is not None
    assert await is_token_revoked(jti, redis_client) is False

    # Revoke token.
    await revoke_token(jti, redis_client)
    assert await redis_client.get(f"{TOKEN_PREFIX}{jti}") is None
    assert await verify_token(token, TokenType.ACCESS, redis=redis_client) is None
    assert await decode_token(token, redis=redis_client) is None
    assert await is_token_revoked(jti, redis_client) is True


async def test_decode_invalid_token(redis_client):
    assert await decode_token("invalid_token", redis=redis_client) is None
