from uuid import uuid4

import pytest
from fastapi import HTTPException

from app.security.policy import get_policy_enforcer
from app.security.rbac import ROLE_PERMISSIONS, Permission, Role


async def test_enforce_sufficient_role(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.ADMIN)
    await policy_enforcer.enforce(user.role, required_role=Role.USER)
    await policy_enforcer.enforce(user.role, required_role=Role.ADMIN)


async def test_enforce_insufficient_role(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.USER)
    with pytest.raises(HTTPException) as exc_info:
        await policy_enforcer.enforce(user.role, required_role=Role.ADMIN)
    assert exc_info.value.status_code == 403
    assert exc_info.value.detail == "Insufficient role"


async def test_enforce_sufficient_permissions(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.MERCHANT)
    await policy_enforcer.enforce(
        user.role,
        required_permissions=[Permission.VIEW_DASHBOARD, Permission.CREATE_INVOICE],
    )


async def test_enforce_insufficient_permissions(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.USER)
    with pytest.raises(HTTPException) as exc_info:
        await policy_enforcer.enforce(user.role, required_permissions=[Permission.MANAGE_USERS])
    assert exc_info.value.status_code == 403
    assert exc_info.value.detail == "Insufficient permissions"


async def test_enforce_admin_all_permissions(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.ADMIN)
    for perm in Permission:
        await policy_enforcer.enforce(user.role, required_permissions=[perm])


async def test_enforce_role_and_permissions(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.SUPPORT)
    await policy_enforcer.enforce(
        user.role,
        required_role=Role.MERCHANT,
        required_permissions=[Permission.VIEW_REPORTS, Permission.CANCEL_INVOICE],
    )


async def test_enforce_insufficient_role_and_permissions(create_test_user):
    policy_enforcer = get_policy_enforcer()
    user = await create_test_user(role=Role.MERCHANT)
    with pytest.raises(HTTPException) as exc_info:
        await policy_enforcer.enforce(
            user.role,
            required_role=Role.SUPPORT,
            required_permissions=[Permission.VIEW_REPORTS],
        )
    assert exc_info.value.status_code == 403
    assert exc_info.value.detail == "Insufficient role"


async def test_role_hierarchy(create_test_user):
    policy_enforcer = get_policy_enforcer()
    for i, role in enumerate(Role.hierarchy):
        unique_email = f"test-{role.value}-{uuid4()}@example.com"
        user = await create_test_user(email=unique_email, role=role)

        for lower_role in Role.hierarchy[: i + 1]:
            await policy_enforcer.enforce(user.role, required_role=lower_role)

        for higher_role in Role.hierarchy[i + 1 :]:
            with pytest.raises(HTTPException):
                await policy_enforcer.enforce(user.role, required_role=higher_role)


async def test_permissions_hierarchy(create_test_user):
    policy_enforcer = get_policy_enforcer()
    for role, permissions in ROLE_PERMISSIONS.items():
        unique_email = f"test-{role.value}-{uuid4()}@example.com"
        user = await create_test_user(email=unique_email, role=role)

        for perm in permissions:
            await policy_enforcer.enforce(user.role, required_permissions=[perm])

        other_permissions = set(Permission) - set(permissions)
        for perm in other_permissions:
            if role != Role.ADMIN:
                with pytest.raises(HTTPException):
                    await policy_enforcer.enforce(user.role, required_permissions=[perm])
