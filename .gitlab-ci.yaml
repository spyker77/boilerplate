# NOTE: Deployment stages are currently disabled for development purposes.
# Re-enable by uncommenting the relevant stages and jobs when ready for deployment.

stages:
  - lint
  - migrate
  - test
  - build
  # - deploy

variables:
  DOCKER_BUILDKIT: 1
  DOCKER_DRIVER: overlay2
  DOCKER_IMAGE: docker:27.1.1
  DOCKER_DIND_IMAGE: docker:27.1.1-dind
  DOCKER_BUILDPLATFORM: linux/amd64,linux/arm64
  # KUBE_CONFIG: $KUBE_CONFIG
  # SENTRY_DSN: $SENTRY_DSN
  # SECRET_KEY_DEV: $SECRET_KEY_DEV
  # SECRET_KEY_PROD: $SECRET_KEY_PROD
  # POSTGRES_USER_DEV: $POSTGRES_USER_DEV
  # POSTGRES_USER_PROD: $POSTGRES_USER_PROD
  # POSTGRES_PASSWORD_DEV: $POSTGRES_PASSWORD_DEV
  # POSTGRES_PASSWORD_PROD: $POSTGRES_PASSWORD_PROD
  # POSTGRES_DB_DEV: $POSTGRES_DB_DEV
  # POSTGRES_DB_PROD: $POSTGRES_DB_PROD
  # POSTGRES_HOST_DEV: $POSTGRES_HOST_DEV
  # POSTGRES_HOST_PROD: $POSTGRES_HOST_PROD
  # DB_HOST_DEV: $DB_HOST_DEV
  # DB_HOST_PROD: $DB_HOST_PROD
  # REDIS_HOST_DEV: $REDIS_HOST_DEV
  # REDIS_HOST_PROD: $REDIS_HOST_PROD
  # RABBIT_HOST_DEV: $RABBIT_HOST_DEV
  # RABBIT_HOST_PROD: $RABBIT_HOST_PROD
  # INGRESS_ROUTE_HOST_DEV: $INGRESS_ROUTE_HOST_DEV
  # INGRESS_ROUTE_HOST_PROD: $INGRESS_ROUTE_HOST_PROD

ruff_lint:
  stage: lint
  image: alpine:3.20.1
  before_script:
    - apk add --no-cache ruff
  script:
    - ruff check .
    - ruff format --check .
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"'

helm_lint:
  stage: lint
  image:
    name: alpine/helm:3.15.2
    entrypoint: [""]
  script:
    - helm lint ./helm -f ./helm/values-dev.yaml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"'

pending_migration:
  stage: migrate
  image: $DOCKER_IMAGE
  services:
    - $DOCKER_DIND_IMAGE
  before_script:
    - touch .env
    - echo "POSTGRES_USER=ci_user" >> .env
    - echo "POSTGRES_PASSWORD=ci_password" >> .env
    - echo "POSTGRES_DB=ci_database" >> .env
    - echo "POSTGRES_HOST=pgbouncer" >> .env
    - echo "POSTGRES_PORT=5432" >> .env
    - echo "REDIS_HOST=redis" >> .env
    - echo "REDIS_PORT=6379" >> .env
    - echo "RABBIT_HOST=rabbitmq" >> .env
    - echo "RABBIT_USER=guest" >> .env
    - echo "AUTH_TYPE=scram-sha-256" >> .env
    - echo "POOL_MODE=session" >> .env
    - echo "MAX_CLIENT_CONN=1000" >> .env
    - echo "DEFAULT_POOL_SIZE=100" >> .env
    - echo "LOG_CONNECTIONS=0" >> .env
    - echo "LOG_DISCONNECTIONS=0" >> .env
    - echo "DB_USER=ci_user" >> .env
    - echo "DB_PASSWORD=ci_password" >> .env
    - echo "DB_NAME=ci_database" >> .env
    - echo "DB_HOST=db" >> .env
  script:
    - docker compose up -d
    - sleep 5
    - docker compose exec -T app alembic check
  after_script:
    - docker compose down -v
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"'
  needs:
    - ruff_lint
    - helm_lint

pytest:
  stage: test
  image: $DOCKER_IMAGE
  services:
    - $DOCKER_DIND_IMAGE
  script:
    - docker compose -f compose.test.yaml up --abort-on-container-exit --exit-code-from app-test
  after_script:
    - docker compose -f compose.test.yaml down -v
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: output/coverage.xml
      junit: output/report.xml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"'
  needs:
    - ruff_lint
    - helm_lint

.build_template: &build_job
  stage: build
  image: $DOCKER_IMAGE
  services:
    - $DOCKER_DIND_IMAGE
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - docker context create buildx-context
    - docker buildx create --use --driver docker-container buildx-context

# TODO: Consider parallel builds for different platforms, plus other optimization techniques.
build_base:
  <<: *build_job
  script:
    - docker buildx build
      --file Dockerfile.base
      --platform $DOCKER_BUILDPLATFORM
      --cache-from type=registry,ref=$CI_REGISTRY_IMAGE:base
      --cache-to type=inline,mode=max
      --tag $CI_REGISTRY_IMAGE:base
      --push
      .
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'
      when: never
  needs:
    - pending_migration
    - pytest

# TODO: Consider parallel builds for different platforms, plus other optimization techniques.
build_dev:
  <<: *build_job
  script:
    - docker buildx build
      --platform $DOCKER_BUILDPLATFORM
      --cache-from type=registry,ref=$CI_REGISTRY_IMAGE:base
      --cache-to type=inline,mode=max
      --build-arg VERSION=latest
      --tag $CI_REGISTRY_IMAGE:latest
      --push
      .
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'
      when: never
  needs:
    - build_base

# deploy_dev:
#   stage: deploy
#   environment:
#     name: dev
#     # url: https://dev.example.com
#   before_script:
#     - echo "$KUBE_CONFIG" > kubeconfig
#     - export KUBECONFIG=./kubeconfig
#   script:
#     - helm upgrade --install boilerplate ./helm -f ./helm/values-dev.yaml
#       --namespace dev
#       --create-namespace
#       --set app.image.repository=$CI_REGISTRY_IMAGE
#       --set app.image.tag=latest
#       --set config.sentryDsn=$SENTRY_DSN
#       --set config.secretKey=$SECRET_KEY_DEV
#       --set postgresql.postgresUser=$POSTGRES_USER_DEV
#       --set postgresql.postgresPassword=$POSTGRES_PASSWORD_DEV
#       --set postgresql.postgresDb=$POSTGRES_DB_DEV
#       --set postgresql.postgresHost=$POSTGRES_HOST_DEV
#       --set pgbouncer.dbHost=$DB_HOST_DEV
#       --set redis.redisHost=$REDIS_HOST_DEV
#       --set rabbitmq.rabbitmqHost=$RABBIT_HOST_DEV
#       --set traefik.ingressRoute.host=$INGRESS_ROUTE_HOST_DEV
#   rules: # we only tag the prod releases, thus any other commit is deployed to dev
#     - if: '$CI_COMMIT_BRANCH == "main"'
#     - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'
#       when: never
#   needs:
#     - build_dev

# TODO: Consider parallel builds for different platforms, plus other optimization techniques.
build_prod:
  <<: *build_job
  script: # in case of a rollback, we reuse existing image with an older tag
    - |
      if docker manifest inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG > /dev/null 2>&1; then
        echo "Image $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG already exists. Skipping build."
      else
        docker buildx build \
          --platform $DOCKER_BUILDPLATFORM \
          --cache-from type=registry,ref=$CI_REGISTRY_IMAGE:base \
          --cache-to type=inline,mode=max \
          --build-arg VERSION=$CI_COMMIT_TAG \
          --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG \
          --tag $CI_REGISTRY_IMAGE:latest \
          --push \
          .
      fi
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'

# deploy_prod:
#   stage: deploy
#   environment:
#     name: prod
#     # url: https://example.com
#   before_script:
#     - echo "$KUBE_CONFIG" > kubeconfig
#     - export KUBECONFIG=./kubeconfig
#   script:
#     - helm upgrade --install boilerplate ./helm -f ./helm/values-prod.yaml
#       --namespace prod
#       --create-namespace
#       --set app.image.repository=$CI_REGISTRY_IMAGE
#       --set app.image.tag=$CI_COMMIT_TAG
#       --set config.sentryDsn=$SENTRY_DSN
#       --set config.secretKey=$SECRET_KEY_PROD
#       --set postgresql.postgresUser=$POSTGRES_USER_PROD
#       --set postgresql.postgresPassword=$POSTGRES_PASSWORD_PROD
#       --set postgresql.postgresDb=$POSTGRES_DB_PROD
#       --set postgresql.postgresHost=$POSTGRES_HOST_PROD
#       --set pgbouncer.dbHost=$DB_HOST_PROD
#       --set redis.redisHost=$REDIS_HOST_PROD
#       --set rabbitmq.rabbitmqHost=$RABBIT_HOST_PROD
#       --set traefik.ingressRoute.host=$INGRESS_ROUTE_HOST_PROD
#   rules:
#     - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'
#   needs:
#     - build_prod
