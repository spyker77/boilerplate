# Database Migrations

There are 2 problems with database migrations:

1. You need to be able to rollback your application without any negative effect.
2. Only one instance of your application should migrate the database at the same time, so replicas may be affected during deployment or rollbacks.

## Migration Which Considers Rollback

The main idea: database migration should be separated from business logic changes in your code to avoid production errors in the case of multiple working instances. Remember that Kubernetes (K8s) deploys/rollbacks applications without degradation, meaning that sometimes you have 2 instances alive, one of which is outdated and the other is new. Thus, one instance will have one schema and another instance will have a different schema with new migrations and code changes.

### You Need to Add a New Column

* **GOOD**
  * Deploy #1: Add a new column, but don't use it anywhere in the code.
  * Deploy #2: Use the new column.
  * In case of a rollback, nothing breaks. You just don't write to the new column, and that's it - you have no errors.
* **BAD**
  * Deploy #1: Add a new column and use it in the code in the same deployment.
  * During a rollback, some of your replicas will still write to this new column and will receive errors because the rollback will remove the column, but at this time, some of the old replicas will still be active.

### You Need to Remove a Column

* **GOOD**
  * Deploy #1: Stop using the column you want to delete.
  * Deploy #2: Remove the column.
  * In case of a rollback, nothing bad happens because nobody uses this old column.
* **BAD**
  * Deploy #1: Stop using the column you want to delete and delete it.
  * In this case, when you deploy the new version, old replicas still exist and they will use a column that doesn't exist, so they will have errors.

### You Need to Change Column Properties

* ⚠️ WARNING: It is recommended not to add any business logic to the database, as it is better to have business logic only in one place - your application. The application should ensure that it doesn't write `null` fields if it should not.
* **GOOD**
  * Deploy #1: Deploy your code so it doesn't allow writing a `null` value.
  * Deploy #2: When you are sure that `null` values can't be written by your app, add the `not null` property to your column.
  * In this case, you will have no degradation in production.
* **BAD**
  * Deploy #1: Deploy your code so it doesn't allow you to write a `null` value and add the `not null` property to the column.
  * Instances that are alive during the deployment will try to write `null` values to your column and will receive errors, which is equivalent to production degradation.

### You Need to Migrate Data from One Table to Another

* **GOOD**
  * Deploy #1: Create a new table.
  * Deploy #2: Start writing to the new table along with the old table (write to both tables), but read only from the old table.
  * Deploy #3: Asynchronously run the process of migrating old data from the old table to the new one.
  * Deploy #4: Start to read data from the new table (here you already have all data migrated from the old table and all new data was written to both tables).
  * Deploy #5: Stop writing to the old table.
  * Deploy #6: Delete the old table.
  * Why so complex? Because if you have a lot of data, your migration will take a long time and will block tables, affecting production and its performance.
* **BAD**
  * Deploy #1: Create a new table, migrate all the data to it, and write new data only to the new table.
  * In this case, if you have a lot of data, your migration and rollback will take a long time and might be painful in case of any errors.

## Migration Using One Instance If You Have a Lot of Replicas in K8s

Currently, we use the `init containers` in Kubernetes (K8s) to ensure that migration is executed only once even if you have multiple replicas. You can place the migration script inside an application image, or you can create a separate image with migrations - it doesn't matter. Here's an example of a Kubernetes `deployment.yaml`:

```yaml
initContainers:
  - name: migrate-db
    image: "{{ .Values.app.image.repository }}:{{ .Values.app.image.tag }}"
    envFrom:
    - secretRef:
        name: boilerplate-app
    command:
      - /bin/sh
      - -c
      - |
        echo "Running database migrations..."
        alembic upgrade head
```
