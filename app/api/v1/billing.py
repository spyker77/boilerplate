from fastapi import APIRouter, Depends, HTTPException, Query, status

from app.models.user import User
from app.schemas.billing import (
    MerchantDetailedStatistics,
    MerchantStatistics,
    OverallStatistics,
    UserDetailedStatistics,
    UserStatistics,
)
from app.security.auth import get_current_user
from app.security.policy import PolicyEnforcer, get_policy_enforcer
from app.security.rbac import Permission
from app.services.billing import BillingService, get_billing_service

router = APIRouter(prefix="/v1/billing", tags=["Billing"])


@router.get("/statistics", response_model=UserStatistics | MerchantStatistics)
async def get_statistics(
    skip: int = Query(0, ge=0),
    limit: int = Query(10, ge=1, le=100),
    current_user: User = Depends(get_current_user),
    billing_service: BillingService = Depends(get_billing_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    """
    Get basic statistics for the current user or merchant.
    """
    try:
        if current_user.is_active_merchant:
            await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.VIEW_MERCHANT_STATISTICS])
            return await billing_service.get_merchant_statistics(current_user.merchant.id)
        else:
            await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.VIEW_USER_STATISTICS])
            return await billing_service.get_user_statistics(current_user.id, skip, limit)
    except ValueError as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))


@router.get("/detailed-statistics", response_model=UserDetailedStatistics | MerchantDetailedStatistics)
async def get_detailed_statistics(
    skip: int = Query(0, ge=0),
    limit: int = Query(10, ge=1, le=100),
    current_user: User = Depends(get_current_user),
    billing_service: BillingService = Depends(get_billing_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    """
    Get detailed statistics for the current user or merchant.
    """
    try:
        if current_user.is_active_merchant:
            await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.VIEW_MERCHANT_STATISTICS])
            return await billing_service.get_merchant_detailed_statistics(current_user.merchant.id)
        else:
            await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.VIEW_USER_STATISTICS])
            return await billing_service.get_user_detailed_statistics(current_user.id, skip, limit)
    except ValueError as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))


@router.get("/overall-statistics", response_model=OverallStatistics)
async def get_overall_statistics(
    current_user: User = Depends(get_current_user),
    billing_service: BillingService = Depends(get_billing_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.VIEW_OVERALL_STATISTICS])

    try:
        return await billing_service.get_overall_statistics()
    except ValueError as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
