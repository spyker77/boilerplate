from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Path, status

from app.models.user import User
from app.schemas.user import UserCreate, UserRead, UserUpdate
from app.security.auth import get_current_user
from app.security.policy import PolicyEnforcer, get_policy_enforcer
from app.security.rbac import Permission, Role
from app.services.user import UserService, get_user_service

router = APIRouter(prefix="/v1/users", tags=["Users"])


@router.post("/register", response_model=UserRead, status_code=status.HTTP_201_CREATED)
async def register_user(data: UserCreate, user_service: UserService = Depends(get_user_service)):
    if await user_service.get_user_by_email(data.email):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email already registered")
    return await user_service.create_user(data)


@router.get("/me", response_model=UserRead)
async def read_me(current_user: User = Depends(get_current_user)):
    return current_user


@router.patch("/me", response_model=UserRead)
async def update_me(
    data: UserUpdate,
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
):
    updated_user = await user_service.update_user(current_user.id, data)
    if not updated_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return updated_user


@router.get("/{user_id}", response_model=UserRead)
async def read_user(
    user_id: Annotated[UUID, Path()],
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
):
    user = await user_service.get_user_by_id(user_id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return user


@router.patch("/{user_id}", response_model=UserRead)
async def update_user(
    user_id: Annotated[UUID, Path()],
    data: UserUpdate,
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_USERS])

    updated_user = await user_service.update_user(user_id, data)
    if not updated_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return updated_user


@router.patch("/{user_id}/deactivate", response_model=UserRead)
async def deactivate_user(
    user_id: Annotated[UUID, Path()],
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_USERS])

    deactivated_user = await user_service.deactivate_user(user_id)
    if not deactivated_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return deactivated_user


@router.patch("/{user_id}/reactivate", response_model=UserRead)
async def reactivate_user(
    user_id: Annotated[UUID, Path()],
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_USERS])

    reactivated_user = await user_service.reactivate_user(user_id)
    if not reactivated_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return reactivated_user


@router.delete(
    "/{user_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="WARNING: Permanently delete user and all the related data.",
    description="""This action is irreversible, use with extreme caution.
    Consider deactivating the user instead for most cases.""",
)
async def hard_delete_user(
    user_id: Annotated[UUID, Path()],
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_role=Role.ADMIN)

    deleted = await user_service.hard_delete_user(user_id)
    if not deleted:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
