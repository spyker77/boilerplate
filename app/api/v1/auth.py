from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Body, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from loguru import logger
from pydantic import EmailStr
from redis.asyncio import Redis

from app.redis import get_redis
from app.security.auth import authenticate_user, create_auth_tokens, refresh_auth_tokens
from app.security.jwt import JSONWebToken, TokenType, create_token, decode_token, revoke_token, verify_token
from app.services.user import UserService, get_user_service

router = APIRouter(prefix="/v1/auth", tags=["Auth"])


@router.post("/login", response_model=JSONWebToken)
async def login(
    form_data: OAuth2PasswordRequestForm = Depends(),
    user_service: UserService = Depends(get_user_service),
    redis: Redis = Depends(get_redis),
):
    user = await authenticate_user(form_data.username, form_data.password, user_service)
    return await create_auth_tokens(user, redis)


@router.post("/logout")
async def logout(refresh_token: Annotated[str, Body(embed=True)], redis: Redis = Depends(get_redis)):
    payload = await decode_token(token=refresh_token, redis=redis)
    if payload and (jti := payload.get("jti")):
        await revoke_token(jti, redis)
        return {"detail": "Successfully logged out"}
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid or expired token")


@router.post("/refresh", response_model=JSONWebToken)
async def refresh_token(
    refresh_token: Annotated[str, Body(embed=True)],
    user_service: UserService = Depends(get_user_service),
    redis: Redis = Depends(get_redis),
):
    return await refresh_auth_tokens(refresh_token, user_service, redis)


@router.post("/request-verify-token")
async def request_verify_token(
    email: Annotated[EmailStr, Body(embed=True)],
    user_service: UserService = Depends(get_user_service),
):
    user = await user_service.get_user_by_email(email)
    if user and not user.is_verified:
        token = await create_token(data={"sub": email}, token_type=TokenType.VERIFY, expires_delta=timedelta(hours=24))
        # TODO: Send this token via email.
        logger.info(f"Verification token for {email}: {token}")
    return {"detail": "If the email is registered and not verified, a verification token has been sent"}


@router.post("/verify")
async def verify_user_with_token(
    token: Annotated[str, Body(embed=True)],
    user_service: UserService = Depends(get_user_service),
):
    email = await verify_token(token, expected_type=TokenType.VERIFY)
    if not email:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid or expired token")

    user = await user_service.get_user_by_email(email)
    if not user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid token")

    if user.is_verified:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User already verified")

    await user_service.verify_user(user)
    return {"detail": "User successfully verified"}


@router.post("/forgot-password")
async def forgot_password(
    email: Annotated[EmailStr, Body(embed=True)],
    user_service: UserService = Depends(get_user_service),
):
    if await user_service.get_user_by_email(email):
        token = await create_token(data={"sub": email}, token_type=TokenType.RESET, expires_delta=timedelta(hours=1))
        # TODO: Send this token via email.
        logger.info(f"Password reset token for {email}: {token}")
    return {"detail": "If the email is registered, a password reset token has been sent"}


@router.post("/reset-password")
async def reset_password(
    token: Annotated[str, Body(embed=True)],
    new_password: Annotated[str, Body(embed=True)],
    user_service: UserService = Depends(get_user_service),
):
    email = await verify_token(token, expected_type=TokenType.RESET)
    if not email:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid or expired token")

    user = await user_service.get_user_by_email(email)
    if not user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid token")

    updated_user = await user_service.reset_password(user, new_password)
    if not updated_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="New password must be different from the current password",
        )

    return {"detail": "Password successfully reset"}
