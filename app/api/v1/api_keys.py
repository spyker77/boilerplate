from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, Path, Query, status

from app.models.user import User
from app.schemas.api_key import APIKeyCreate, APIKeyList, APIKeyRead
from app.security.auth import get_current_user
from app.security.policy import PolicyEnforcer, get_policy_enforcer
from app.security.rbac import Permission
from app.services.api_key import APIKeyService, get_api_key_service

router = APIRouter(prefix="/v1/api-keys", tags=["API Keys"])


@router.get("/", response_model=APIKeyList)
async def list_api_keys(
    skip: int = Query(0, ge=0),
    limit: int = Query(10, ge=1, le=100),
    current_user: User = Depends(get_current_user),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_API_KEYS])

    return await api_key_service.list_active_api_keys(current_user.id, skip, limit)


@router.post("/", response_model=APIKeyRead)
async def create_api_key(
    api_key_data: APIKeyCreate,
    current_user: User = Depends(get_current_user),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_API_KEYS])

    return await api_key_service.create_api_key(current_user.id, api_key_data.name, api_key_data.expires_in_days)


@router.get("/{api_key}", response_model=APIKeyRead)
async def get_api_key(
    api_key: Annotated[str, Path(min_length=32, max_length=64)],
    current_user: User = Depends(get_current_user),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_API_KEYS])

    api_key_obj = await api_key_service.get_api_key(api_key)
    if not api_key_obj:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="API key not found")
    return api_key_obj


@router.post("/{api_key}/rotate", response_model=APIKeyRead)
async def rotate_api_key(
    api_key: Annotated[str, Path(min_length=32, max_length=64)],
    current_user: User = Depends(get_current_user),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_API_KEYS])

    new_api_key = await api_key_service.rotate_api_key(api_key)
    if not new_api_key:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="API key not found")
    return new_api_key


@router.patch("/{api_key}/deactivate")
async def deactivate_api_key(
    api_key: Annotated[str, Path(min_length=32, max_length=64)],
    current_user: User = Depends(get_current_user),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    policy_enforcer: PolicyEnforcer = Depends(get_policy_enforcer),
):
    await policy_enforcer.enforce(current_user.role, required_permissions=[Permission.MANAGE_API_KEYS])

    success = await api_key_service.deactivate_api_key(api_key, current_user.id)
    if not success:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="API key not found")
    return {"message": "API key deactivated successfully"}
