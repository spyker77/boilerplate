from typing import Annotated

from fastapi import APIRouter, Body, Depends, HTTPException, status
from redis.asyncio import Redis

from app.models.user import User
from app.redis import get_redis
from app.schemas.user import UserUpdate
from app.security.auth import authenticate_user, create_auth_tokens, get_current_user
from app.security.jwt import JSONWebToken
from app.security.two_factor import TwoFactorSetup, generate_2fa_secret, generate_backup_codes, verify_2fa_code
from app.services.user import UserService, get_user_service

router = APIRouter(prefix="/v1/2fa", tags=["2FA"])


@router.post("/enable", response_model=TwoFactorSetup)
async def enable_2fa(
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
):
    if current_user.two_factor_enabled:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="2FA is already enabled")
    return await generate_2fa_secret(current_user, user_service)


@router.post("/verify")
async def verify_2fa(code: Annotated[str, Body(embed=True)], current_user: User = Depends(get_current_user)):
    if verify_2fa_code(current_user, code):
        return {"message": "2FA verification successful"}
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid 2FA code")


@router.post("/complete", response_model=JSONWebToken)
async def complete_2fa(
    code: Annotated[str, Body(embed=True)],
    current_user: User = Depends(get_current_user),
    redis: Redis = Depends(get_redis),
):
    if verify_2fa_code(current_user, code):
        return await create_auth_tokens(current_user, redis)
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid 2FA code")


@router.post("/disable")
async def disable_2fa(
    password: Annotated[str, Body(embed=True)],
    code: Annotated[str, Body(embed=True)],
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
):
    # An extra layer of security in case of stolen auth token or session hijacking.
    await authenticate_user(current_user.email, password, user_service)

    if not verify_2fa_code(current_user, code):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid 2FA code")

    await user_service.update_user(current_user.id, UserUpdate(two_factor_enabled=False, two_factor_secret=None))

    # TODO: Send notification email to user about 2FA being disabled.
    # TODO: Log this action for security auditing.

    return {"message": "2FA has been disabled"}


@router.post("/generate-backup-codes", response_model=list[str])
async def generate_backup_codes_for_2fa(
    current_user: User = Depends(get_current_user),
    user_service: UserService = Depends(get_user_service),
):
    if not current_user.two_factor_enabled:
        raise HTTPException(status_code=400, detail="2FA is not enabled")
    return await generate_backup_codes(current_user, user_service)
