from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict

ENV_PATH = ".env"


class MiscConf(BaseSettings):
    DB_URL: str = Field(default="")
    SECRET_KEY: str = Field(default="")
    SENTRY_DSN: str = Field(default="")
    BINANCE_API_URL: str = Field(default="")
    API_KEY_CACHE_EXPIRY_SECONDS: int = Field(default=3600)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = Field(default=30)
    REFRESH_TOKEN_EXPIRE_DAYS: int = Field(default=30)
    TASKIQ_SCHEDULER_ENABLED: bool = Field(default=False)
    ENVIRONMENT: str = Field(default="dev")
    DEBUG: bool = Field(default=False)
    ORIGINS: list[str] = Field(default=["http://localhost", "https://localhost"])


MISC_CONF = MiscConf()


class PostgresConf(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=ENV_PATH, env_prefix="POSTGRES_", extra="ignore", env_file_encoding="utf-8"
    )

    USER: str = Field(default="")
    PASSWORD: str = Field(default="")
    DB: str = Field(default="")
    HOST: str = Field(default="")
    PORT: int = Field(default=5432)

    @property
    def URL(self) -> str:
        return f"postgresql+asyncpg://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.DB}"


DB_URL = MISC_CONF.DB_URL or PostgresConf().URL


class RedisConf(BaseSettings):
    model_config = SettingsConfigDict(env_file=ENV_PATH, env_prefix="REDIS_", extra="ignore", env_file_encoding="utf-8")

    HOST: str = Field(default="")
    PORT: int = Field(default=5672)


REDIS_CONF = RedisConf()


class RabbitConf(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=ENV_PATH, env_prefix="RABBIT_", extra="ignore", env_file_encoding="utf-8"
    )
    HOST: str = Field(default="")
    USER: str = Field(default="")
    PASSWORD: str = Field(default="")
    PORT: int = Field(default=5672)

    @property
    def URL(self) -> str:
        return f"pyamqp://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/"


RABBIT_CONF = RabbitConf()
