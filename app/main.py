from contextlib import asynccontextmanager

import sentry_sdk
from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app import __version__
from app.api.v1.api_keys import router as api_keys_router_v1
from app.api.v1.auth import router as auth_router_v1
from app.api.v1.billing import router as billing_router_v1
from app.api.v1.two_factor import router as two_factor_router_v1
from app.api.v1.user import router as user_router_v1
from app.config import MISC_CONF
from app.taskiq import broker

sentry_sdk.init(
    dsn=MISC_CONF.SENTRY_DSN,
    environment=MISC_CONF.ENVIRONMENT,
    debug=MISC_CONF.DEBUG,
    traces_sample_rate=1.0,
)


@asynccontextmanager
async def lifespan(app: FastAPI):
    if not broker.is_worker_process:
        await broker.startup()

    yield

    if not broker.is_worker_process:
        await broker.shutdown()


app = FastAPI(
    title="Boilerplate",
    summary="Crypto Payment Gateway ⛩️",
    description="""On this page you can find more information about the possibilities when working with the Crypto API.
    You can also see examples of requests and responses from the server.""",
    version=__version__,
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
    debug=MISC_CONF.DEBUG,
    lifespan=lifespan,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=MISC_CONF.ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Top level router with a common prefix – can be replaced by the app directly if host will be like `api.example.com`
router = APIRouter(prefix="/api")
router.include_router(auth_router_v1)
router.include_router(two_factor_router_v1)
router.include_router(api_keys_router_v1)
router.include_router(user_router_v1)
router.include_router(billing_router_v1)

# Include all the routers under a common prefix.
app.include_router(router)
