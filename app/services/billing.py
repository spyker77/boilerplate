from decimal import Decimal
from typing import Any
from uuid import UUID

from fastapi import Depends
from sqlalchemy import func, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_db
from app.models.api_key import APIKey
from app.models.blockchain import BlockchainTransaction, BlockchainTransactionStatus
from app.models.integration import Integration
from app.models.invoice import Invoice, InvoiceStatus
from app.models.merchant import Merchant
from app.models.payment import Payment, PaymentStatus
from app.models.payout import Payout, PayoutStatus
from app.models.user import User
from app.models.wallet import Wallet
from app.models.webhook import Webhook


class BillingService:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def get_merchant_statistics(self, merchant_id: UUID) -> dict[str, Decimal | int]:
        try:
            total_payments_result = await self.db.execute(
                select(func.sum(Payment.amount)).where(
                    Payment.merchant_id == merchant_id,
                    Payment.status == PaymentStatus.COMPLETED,
                )
            )
            total_payments = total_payments_result.scalar_one_or_none() or Decimal("0")

            total_payouts_result = await self.db.execute(
                select(func.sum(Payout.amount)).where(
                    Payout.merchant_id == merchant_id,
                    Payout.status == PayoutStatus.COMPLETED,
                )
            )
            total_payouts = total_payouts_result.scalar_one_or_none() or Decimal("0")

            successful_transactions_result = await self.db.execute(
                select(func.count(Payment.id)).where(
                    Payment.merchant_id == merchant_id,
                    Payment.status == PaymentStatus.COMPLETED,
                )
            )
            successful_transactions = successful_transactions_result.scalar_one_or_none() or 0

            failed_transactions_result = await self.db.execute(
                select(func.count(Payment.id)).where(
                    Payment.merchant_id == merchant_id,
                    Payment.status == PaymentStatus.FAILED,
                )
            )
            failed_transactions = failed_transactions_result.scalar_one_or_none() or 0

            pending_transactions_result = await self.db.execute(
                select(func.count(Payment.id)).where(
                    Payment.merchant_id == merchant_id,
                    Payment.status == PaymentStatus.PENDING,
                )
            )
            pending_transactions = pending_transactions_result.scalar_one_or_none() or 0

            active_integrations_result = await self.db.execute(
                select(func.count(Integration.id)).where(
                    Integration.merchant_id == merchant_id,
                    Integration.is_active,
                )
            )
            active_integrations = active_integrations_result.scalar_one_or_none() or 0

            return {
                "total_payments": total_payments,
                "total_payouts": total_payouts,
                "successful_transactions": successful_transactions,
                "failed_transactions": failed_transactions,
                "pending_transactions": pending_transactions,
                "balance": total_payments - total_payouts,
                "active_integrations": active_integrations,
            }
        except SQLAlchemyError as e:
            raise ValueError(f"Error fetching merchant statistics: {str(e)}")

    async def get_merchant_detailed_statistics(self, merchant_id: UUID) -> dict[str, Any]:
        try:
            basic_merchant_stats = await self.get_merchant_statistics(merchant_id)

            total_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(Invoice.merchant_id == merchant_id)
            )
            total_invoices = total_invoices_result.scalar_one_or_none() or 0

            paid_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(
                    Invoice.merchant_id == merchant_id, Invoice.status == InvoiceStatus.PAID
                )
            )
            paid_invoices = paid_invoices_result.scalar_one_or_none() or 0

            pending_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(
                    Invoice.merchant_id == merchant_id, Invoice.status == InvoiceStatus.PENDING
                )
            )
            pending_invoices = pending_invoices_result.scalar_one_or_none() or 0

            total_api_keys_result = await self.db.execute(
                select(func.count(APIKey.id)).where(APIKey.merchant_id == merchant_id)
            )
            total_api_keys = total_api_keys_result.scalar_one_or_none() or 0

            active_api_keys_result = await self.db.execute(
                select(func.count(APIKey.id)).where(APIKey.merchant_id == merchant_id, APIKey.is_active)
            )
            active_api_keys = active_api_keys_result.scalar_one_or_none() or 0

            webhook_count_result = await self.db.execute(
                select(func.count(Webhook.id)).where(Webhook.merchant_id == merchant_id)
            )
            webhook_count = webhook_count_result.scalar_one_or_none() or 0

            failed_webhook_count_result = await self.db.execute(
                select(func.count(Webhook.id)).where(Webhook.merchant_id == merchant_id, Webhook.failure_count > 0)
            )
            failed_webhook_count = failed_webhook_count_result.scalar_one_or_none() or 0

            return {
                **basic_merchant_stats,
                "total_invoices": total_invoices,
                "paid_invoices": paid_invoices,
                "pending_invoices": pending_invoices,
                "total_api_keys": total_api_keys,
                "active_api_keys": active_api_keys,
                "webhook_count": webhook_count,
                "failed_webhook_count": failed_webhook_count,
            }
        except SQLAlchemyError as e:
            raise ValueError(f"Error fetching detailed merchant statistics: {str(e)}")

    async def get_user_statistics(
        self,
        user_id: UUID,
        skip: int = 0,
        limit: int = 20,
    ) -> dict[str, Decimal | int | list[dict]]:
        try:
            total_payments_result = await self.db.execute(
                select(func.sum(Payment.amount)).where(
                    Payment.user_id == user_id,
                    Payment.status == PaymentStatus.COMPLETED,
                )
            )
            total_payments = total_payments_result.scalar_one_or_none() or Decimal("0")

            successful_transactions_result = await self.db.execute(
                select(func.count(Payment.id)).where(
                    Payment.user_id == user_id,
                    Payment.status == PaymentStatus.COMPLETED,
                )
            )
            successful_transactions = successful_transactions_result.scalar_one_or_none() or 0

            latest_transactions_result = await self.db.execute(
                select(Payment)
                .where(Payment.user_id == user_id)
                .order_by(Payment.created_at.desc())
                .offset(skip)
                .limit(limit)
            )
            latest_transactions = latest_transactions_result.scalars().all()

            total_balance_result = await self.db.execute(
                select(func.sum(Wallet.balance)).where(Wallet.user_id == user_id)
            )
            total_balance = total_balance_result.scalar_one_or_none() or Decimal("0")

            wallets_result = await self.db.execute(select(Wallet).where(Wallet.user_id == user_id))
            wallets = wallets_result.scalars().all()

            return {
                "total_payments": total_payments,
                "successful_transactions": successful_transactions,
                "latest_transactions": [
                    {
                        "id": str(tx.id),
                        "amount": tx.amount,
                        "currency": tx.currency.code,
                        "status": tx.status.value,
                        "created_at": tx.created_at.isoformat(),
                        "blockchain_transaction": {
                            "transaction_hash": tx.blockchain_transaction.transaction_hash,
                            "status": tx.blockchain_transaction.status.value,
                            "block_number": tx.blockchain_transaction.block_number,
                            "from_address": tx.blockchain_transaction.from_address,
                            "to_address": tx.blockchain_transaction.to_address,
                        }
                        if tx.blockchain_transaction
                        else None,
                    }
                    for tx in latest_transactions
                ],
                "total_balance": total_balance,
                "wallets": [
                    {
                        "currency": wallet.currency.code,
                        "balance": wallet.balance,
                        "address": wallet.address,
                    }
                    for wallet in wallets
                ],
            }
        except SQLAlchemyError as e:
            raise ValueError(f"Error fetching user statistics: {str(e)}")

    async def get_user_detailed_statistics(self, user_id: UUID, skip: int = 0, limit: int = 20) -> dict[str, Any]:
        try:
            basic_user_stats = await self.get_user_statistics(user_id, limit, skip)

            total_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(Invoice.user_id == user_id)
            )
            total_invoices = total_invoices_result.scalar_one_or_none() or 0

            paid_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(Invoice.user_id == user_id, Invoice.status == InvoiceStatus.PAID)
            )
            paid_invoices = paid_invoices_result.scalar_one_or_none() or 0

            pending_invoices_result = await self.db.execute(
                select(func.count(Invoice.id)).where(
                    Invoice.user_id == user_id, Invoice.status == InvoiceStatus.PENDING
                )
            )
            pending_invoices = pending_invoices_result.scalar_one_or_none() or 0

            return {
                **basic_user_stats,
                "total_invoices": total_invoices,
                "paid_invoices": paid_invoices,
                "pending_invoices": pending_invoices,
            }
        except SQLAlchemyError as e:
            raise ValueError(f"Error fetching user detailed statistics: {str(e)}")

    async def get_overall_statistics(self) -> dict[str, Decimal | int]:
        try:
            total_merchants_result = await self.db.execute(select(func.count(Merchant.id)))
            total_merchants = total_merchants_result.scalar_one_or_none() or 0

            total_payments_result = await self.db.execute(
                select(func.sum(Payment.amount)).where(Payment.status == PaymentStatus.COMPLETED)
            )
            total_payments = total_payments_result.scalar_one_or_none() or Decimal("0")

            total_payouts_result = await self.db.execute(
                select(func.sum(Payout.amount)).where(Payout.status == PayoutStatus.COMPLETED)
            )
            total_payouts = total_payouts_result.scalar_one_or_none() or Decimal("0")

            active_merchants_result = await self.db.execute(select(func.count(Merchant.id)).where(Merchant.is_active))
            active_merchants = active_merchants_result.scalar_one_or_none() or 0

            total_users_result = await self.db.execute(select(func.count(User.id)))
            total_users = total_users_result.scalar_one_or_none() or 0

            total_blockchain_transactions_result = await self.db.execute(select(func.count(BlockchainTransaction.id)))
            total_blockchain_transactions = total_blockchain_transactions_result.scalar_one_or_none() or 0

            successful_blockchain_transactions_result = await self.db.execute(
                select(func.count(BlockchainTransaction.id)).where(
                    BlockchainTransaction.status == BlockchainTransactionStatus.CONFIRMED
                )
            )
            successful_blockchain_transactions = successful_blockchain_transactions_result.scalar_one_or_none() or 0

            failed_blockchain_transactions_result = await self.db.execute(
                select(func.count(BlockchainTransaction.id)).where(
                    BlockchainTransaction.status == BlockchainTransactionStatus.FAILED
                )
            )
            failed_blockchain_transactions = failed_blockchain_transactions_result.scalar_one_or_none() or 0

            return {
                "total_merchants": total_merchants,
                "active_merchants": active_merchants,
                "total_users": total_users,
                "total_payments": total_payments,
                "total_payouts": total_payouts,
                "platform_revenue": total_payments - total_payouts,
                "total_blockchain_transactions": total_blockchain_transactions,
                "successful_blockchain_transactions": successful_blockchain_transactions,
                "failed_blockchain_transactions": failed_blockchain_transactions,
            }
        except SQLAlchemyError as e:
            raise ValueError(f"Error fetching overall statistics: {str(e)}")


async def get_billing_service(db: AsyncSession = Depends(get_db)) -> BillingService:
    return BillingService(db)
