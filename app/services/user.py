from uuid import UUID

from fastapi import Depends
from loguru import logger
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_db
from app.models.user import User
from app.schemas.user import UserCreate, UserUpdate
from app.security.password import get_password_hash, needs_rehash, verify_password


class UserService:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def get_user_by_id(self, user_id: UUID) -> User | None:
        result = await self.db.execute(select(User).filter(User.id == user_id))
        return result.scalar_one_or_none()

    async def get_user_by_email(self, email: str) -> User | None:
        result = await self.db.execute(select(User).filter(User.email == email))
        return result.scalar_one_or_none()

    async def create_user(self, data: UserCreate) -> User:
        hashed_password = get_password_hash(data.password)
        user = User(
            email=data.email,
            hashed_password=hashed_password,
            first_name=data.first_name,
            last_name=data.last_name,
            role=data.role,
        )
        self.db.add(user)
        await self.db.commit()
        await self.db.refresh(user)

        logger.info(f"User {user.id} has been created")
        return user

    async def update_user(self, user_id: UUID, data: UserUpdate) -> User | None:
        user = await self.get_user_by_id(user_id)
        if not user:
            return None

        update_data = data.model_dump(exclude_unset=True)
        if "password" in update_data:
            update_data["hashed_password"] = get_password_hash(update_data.pop("password"))

        await self.db.execute(update(User).where(User.id == user_id).values(**update_data))
        await self.db.commit()
        await self.db.refresh(user)

        logger.info(f"User {user_id} has been updated")
        return user

    async def hard_delete_user(self, user_id: UUID) -> bool:
        """
        Permanently delete a user and all the related data.

        WARNING: This action is irreversible, use with extreme caution.
        Consider using `deactivate_user` instead for most cases.
        """
        user = await self.get_user_by_id(user_id)
        if not user:
            return False

        await self.db.delete(user)
        await self.db.commit()

        logger.info(f"User {user_id} has been deleted")
        return True

    async def deactivate_user(self, user_id: UUID) -> User | None:
        user = await self.get_user_by_id(user_id)
        if not user:
            return None

        updated_user = await self.update_user(user.id, UserUpdate(is_active=False))
        logger.info(f"User {user_id} has been deactivated")
        return updated_user

    async def reactivate_user(self, user_id: UUID) -> User | None:
        user = await self.get_user_by_id(user_id)
        if not user:
            return None

        updated_user = await self.update_user(user.id, UserUpdate(is_active=True))
        logger.info(f"User {user_id} has been reactivated")
        return updated_user

    async def verify_user(self, user: User) -> User | None:
        if user.is_verified:
            return None

        user.is_verified = True
        await self.db.commit()
        await self.db.refresh(user)

        logger.info(f"User {user.id} has been verified")
        return user

    async def verify_and_update_password(self, user: User, password: str) -> bool:
        if not verify_password(password, user.hashed_password):
            return False

        if needs_rehash(user.hashed_password):
            user.hashed_password = get_password_hash(password)
            await self.db.commit()
            logger.info(f"Password hash updated for user {user.id}")

        return True

    async def reset_password(self, user: User, new_password: str) -> User | None:
        # New password must be different from the current password.
        if verify_password(new_password, user.hashed_password):
            return None

        user.hashed_password = get_password_hash(new_password)
        await self.db.commit()
        await self.db.refresh(user)

        logger.info(f"User {user.id} has reset the password")
        return user


async def get_user_service(db: AsyncSession = Depends(get_db)) -> UserService:
    return UserService(db)
