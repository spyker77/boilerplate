import secrets
from collections.abc import Sequence
from datetime import UTC, datetime, timedelta
from uuid import UUID

from fastapi import Depends
from loguru import logger
from redis.asyncio import Redis
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import MISC_CONF
from app.db import get_db
from app.models.api_key import APIKey
from app.redis import get_redis
from app.schemas.api_key import APIKeyRead
from app.utils.serializers import json_deserializer, json_serializer


class APIKeyService:
    CACHE_TTL = MISC_CONF.API_KEY_CACHE_EXPIRY_SECONDS
    API_KEY_CACHE_PREFIX = "api_key:"

    def __init__(self, db: AsyncSession, redis: Redis):
        self.db = db
        self.redis = redis

    @staticmethod
    def generate_api_key() -> str:
        return secrets.token_urlsafe(32)

    async def get_api_key(self, key: str) -> APIKey | None:
        # Try to get the API key from cache.
        cache_key = f"{self.API_KEY_CACHE_PREFIX}{key}"
        if cached_key := await self.redis.get(cache_key):
            api_key_data = json_deserializer(cached_key)
            api_key_read = APIKeyRead.model_validate(api_key_data)
            api_key = APIKey(**api_key_read.model_dump())
        else:
            # If not in cache, get from database.
            result = await self.db.execute(select(APIKey).where(APIKey.key == key, APIKey.is_active))
            if api_key := result.scalar_one_or_none():
                await self._cache_api_key(api_key)

        if api_key and secrets.compare_digest(api_key.key, key):  # prevent timing attacks
            if api_key.expires_at and api_key.expires_at < datetime.now(UTC):
                await self.deactivate_api_key(api_key.key, api_key.user_id)
                return None
            return api_key

        return None

    async def create_api_key(self, user_id: UUID, name: str, expires_in_days: int | None = None) -> APIKey:
        key = self.generate_api_key()
        expires_at = datetime.now(UTC) + timedelta(days=expires_in_days) if expires_in_days else None

        api_key = APIKey(key=key, name=name, user_id=user_id, expires_at=expires_at)
        self.db.add(api_key)
        await self.db.commit()
        await self.db.refresh(api_key)

        await self._cache_api_key(api_key)

        logger.info(f"Created new API key {api_key.id} for user {user_id}")
        return api_key

    async def list_active_api_keys(self, user_id: UUID, skip: int = 0, limit: int = 20) -> Sequence[APIKey]:
        result = await self.db.execute(
            select(APIKey)
            .where(
                APIKey.user_id == user_id,
                APIKey.is_active,
            )
            .offset(skip)
            .limit(limit)
        )
        return result.scalars().all()

    async def deactivate_api_key(self, key: str, user_id: UUID) -> bool:
        result = await self.db.execute(select(APIKey).where(APIKey.key == key, APIKey.user_id == user_id))
        if api_key := result.scalar_one_or_none():
            api_key.is_active = False
            await self.db.commit()
            await self.db.refresh(api_key)

            await self._invalidate_api_key_cache(api_key.key)
            logger.info(f"Deactivated API key {api_key.id}")
            return True

        logger.warning(f"Failed to deactivate API key {key} for user {user_id}")
        return False

    async def rotate_api_key(self, key: str) -> APIKey | None:
        result = await self.db.execute(select(APIKey).where(APIKey.key == key))
        api_key = result.scalar_one_or_none()
        if not api_key:
            return None

        old_key = api_key.key
        new_key = self.generate_api_key()
        api_key.key = new_key
        api_key.rotated_at = datetime.now(UTC)
        await self.db.commit()
        await self.db.refresh(api_key)

        await self._invalidate_api_key_cache(old_key)
        await self._cache_api_key(api_key)

        logger.info(f"Rotated API key {api_key.id}")
        return api_key

    async def _cache_api_key(self, api_key: APIKey):
        cache_key = f"{self.API_KEY_CACHE_PREFIX}{api_key.key}"
        cache_expiry = (
            int((api_key.expires_at - datetime.now(UTC)).total_seconds()) if api_key.expires_at else self.CACHE_TTL
        )
        api_key_read = APIKeyRead.model_validate(api_key)
        serialized_data = json_serializer(api_key_read.model_dump())
        await self.redis.setex(cache_key, cache_expiry, serialized_data)

    async def _invalidate_api_key_cache(self, key: str):
        cache_key = f"{self.API_KEY_CACHE_PREFIX}{key}"
        await self.redis.delete(cache_key)


async def get_api_key_service(db: AsyncSession = Depends(get_db), redis: Redis = Depends(get_redis)) -> APIKeyService:
    return APIKeyService(db, redis)
