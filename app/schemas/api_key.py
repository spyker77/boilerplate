from datetime import datetime

from pydantic import UUID4, BaseModel, ConfigDict, Field, RootModel


class APIKeyCreate(BaseModel):
    name: str = Field(..., min_length=1, max_length=50)
    expires_in_days: int | None = Field(None, ge=1)


class APIKeyRead(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: UUID4
    key: str
    name: str
    is_active: bool
    expires_at: datetime | None = None
    rotated_at: datetime
    user_id: UUID4
    merchant_id: datetime | None = None


class APIKeyList(RootModel):
    root: list[APIKeyRead | None] = []
