from pydantic import UUID4, BaseModel, ConfigDict, EmailStr

from app.security.rbac import Role


class UserCreate(BaseModel):
    email: EmailStr
    password: str
    first_name: str
    last_name: str
    role: Role = Role.USER


class UserRead(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: UUID4
    email: EmailStr
    first_name: str
    last_name: str
    role: Role
    is_active: bool
    is_verified: bool
    is_superuser: bool
    is_merchant: bool
    two_factor_enabled: bool


class UserUpdate(BaseModel):
    first_name: str | None = None
    last_name: str | None = None
    password: str | None = None
    role: Role | None = None
    is_active: bool | None = None
    is_verified: bool | None = None
    two_factor_enabled: bool | None = None
    two_factor_secret: str | None = None
    backup_codes: list[str] | None = None
