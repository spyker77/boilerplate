from datetime import datetime
from decimal import Decimal
from uuid import UUID

from pydantic import BaseModel

from app.models.blockchain import BlockchainTransactionStatus
from app.models.payment import PaymentStatus


class BlockchainTransactionInfo(BaseModel):
    transaction_hash: str
    status: BlockchainTransactionStatus
    block_number: int | None
    from_address: str
    to_address: str


class PaymentInfo(BaseModel):
    id: UUID
    amount: Decimal
    currency: str
    status: PaymentStatus
    created_at: datetime
    blockchain_transaction: BlockchainTransactionInfo | None


class WalletInfo(BaseModel):
    currency: str
    balance: Decimal
    address: str


class UserStatistics(BaseModel):
    total_payments: Decimal
    successful_transactions: int
    latest_transactions: list[PaymentInfo]
    total_balance: Decimal
    wallets: list[WalletInfo]


class MerchantStatistics(BaseModel):
    total_payments: Decimal
    total_payouts: Decimal
    successful_transactions: int
    failed_transactions: int
    pending_transactions: int
    balance: Decimal
    active_integrations: int


class OverallStatistics(BaseModel):
    total_merchants: int
    active_merchants: int
    total_users: int
    total_payments: Decimal
    total_payouts: Decimal
    platform_revenue: Decimal
    total_blockchain_transactions: int
    successful_blockchain_transactions: int
    failed_blockchain_transactions: int


class UserDetailedStatistics(UserStatistics):
    total_invoices: int
    paid_invoices: int
    pending_invoices: int


class MerchantDetailedStatistics(MerchantStatistics):
    total_invoices: int
    paid_invoices: int
    pending_invoices: int
    total_api_keys: int
    active_api_keys: int
    webhook_count: int
    failed_webhook_count: int
