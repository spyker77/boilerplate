from datetime import UTC, datetime
from enum import StrEnum
from uuid import UUID

from sqlalchemy import JSON, Boolean, DateTime, Enum, ForeignKey, Integer, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class WebhookEventType(StrEnum):
    PAYMENT_CREATED = "payment_created"
    PAYMENT_COMPLETED = "payment_completed"
    PAYMENT_FAILED = "payment_failed"

    PAYOUT_CREATED = "payout_created"
    PAYOUT_COMPLETED = "payout_completed"
    PAYOUT_FAILED = "payout_failed"

    EXCHANGE_RATE_UPDATED = "exchange_rate_updated"


class Webhook(AbstractBase):
    __tablename__ = "webhooks"

    url: Mapped[str] = mapped_column(String(255), nullable=False, index=True)
    event_types: Mapped[list[WebhookEventType]] = mapped_column(String(255), nullable=False, index=True)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    secret_key: Mapped[str] = mapped_column(String(64), nullable=False)
    last_triggered_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    failure_count: Mapped[int] = mapped_column(Integer, default=0)
    merchant_id: Mapped[UUID] = mapped_column(ForeignKey("merchants.id"))

    merchant: Mapped["Merchant"] = relationship(back_populates="webhooks")
    logs: Mapped[list["WebhookLog"]] = relationship(back_populates="webhook", cascade="all, delete-orphan")


class WebhookLog(AbstractBase):
    __tablename__ = "webhook_logs"

    event_type: Mapped[WebhookEventType] = mapped_column(Enum(WebhookEventType), nullable=False, index=True)
    payload: Mapped[dict] = mapped_column(JSON, nullable=False)
    response_status_code: Mapped[int | None] = mapped_column(Integer)
    response_body: Mapped[str | None] = mapped_column(String)
    sent_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), default=lambda: datetime.now(UTC))
    webhook_id: Mapped[UUID] = mapped_column(ForeignKey("webhooks.id"))

    webhook: Mapped[Webhook] = relationship(back_populates="logs")
