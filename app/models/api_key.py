from datetime import UTC, datetime
from uuid import UUID

from sqlalchemy import Boolean, DateTime, ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class APIKey(AbstractBase):
    __tablename__ = "api_keys"

    key: Mapped[str] = mapped_column(String(length=64), unique=True, index=True)
    name: Mapped[str] = mapped_column(String(length=50))
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    expires_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    rotated_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), default=lambda: datetime.now(UTC))
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))
    merchant_id: Mapped[UUID | None] = mapped_column(ForeignKey("merchants.id"))

    user: Mapped["User"] = relationship(back_populates="api_keys")
    merchant: Mapped["Merchant | None"] = relationship(back_populates="api_keys")
