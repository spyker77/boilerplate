from uuid import UUID, uuid4

from sqlalchemy import Uuid
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from app.models._mixins import TimestampMixin


class Base(DeclarativeBase):
    pass


class AbstractBase(TimestampMixin, Base):
    __abstract__ = True

    id: Mapped[UUID] = mapped_column(Uuid(as_uuid=True), primary_key=True, default=uuid4)
