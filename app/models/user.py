from sqlalchemy import ARRAY, Boolean, Enum, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase
from app.security.rbac import Role


class User(AbstractBase):
    __tablename__ = "users"

    email: Mapped[str] = mapped_column(String(length=320), unique=True, nullable=False, index=True)
    hashed_password: Mapped[str] = mapped_column(String(length=1024), nullable=False)
    first_name: Mapped[str] = mapped_column(String(length=50), nullable=False, index=True)
    last_name: Mapped[str] = mapped_column(String(length=50), nullable=False, index=True)
    role: Mapped[Role] = mapped_column(Enum(Role), default=Role.USER)

    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    is_verified: Mapped[bool] = mapped_column(Boolean, default=False)
    is_superuser: Mapped[bool] = mapped_column(Boolean, default=False)
    is_merchant: Mapped[bool] = mapped_column(Boolean, default=False)

    two_factor_enabled: Mapped[bool] = mapped_column(Boolean, default=False)
    two_factor_secret: Mapped[str | None] = mapped_column(String(32), nullable=True)
    backup_codes: Mapped[list[str] | None] = mapped_column(ARRAY(String(8)), nullable=True)

    api_keys: Mapped[list["APIKey"]] = relationship(back_populates="user", cascade="all, delete-orphan")
    invoices: Mapped[list["Invoice"]] = relationship(back_populates="user", cascade="all, delete-orphan")
    merchant: Mapped["Merchant | None"] = relationship(back_populates="user")
    payments: Mapped[list["Payment"]] = relationship(back_populates="user", cascade="all, delete-orphan")
    payouts: Mapped[list["Payout"]] = relationship(back_populates="user", cascade="all, delete-orphan")
    wallets: Mapped[list["Wallet"]] = relationship(back_populates="user", cascade="all, delete-orphan")

    @property
    def full_name(self) -> str:
        return f"{self.first_name} {self.last_name}"

    @property
    def is_active_merchant(self) -> bool:
        return self.is_merchant and self.merchant is not None and self.merchant.is_active
