from sqlalchemy import Boolean, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class Partner(AbstractBase):
    __tablename__ = "partners"

    name: Mapped[str] = mapped_column(String, nullable=False, index=True)
    website: Mapped[str] = mapped_column(String(200), index=True)
    api_key: Mapped[str] = mapped_column(String, nullable=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    description: Mapped[str | None] = mapped_column(String, nullable=True)

    integrations: Mapped[list["Integration"]] = relationship(back_populates="partner", cascade="all, delete-orphan")
