from decimal import Decimal
from uuid import UUID

from sqlalchemy import ForeignKey, Numeric
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class ExchangeRate(AbstractBase):
    __tablename__ = "exchange_rates"

    rate: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8))
    base_currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))
    quote_currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))

    base_currency: Mapped["Currency"] = relationship(foreign_keys=[base_currency_id])
    quote_currency: Mapped["Currency"] = relationship(foreign_keys=[quote_currency_id])
