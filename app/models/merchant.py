from uuid import UUID

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class Merchant(AbstractBase):
    __tablename__ = "merchants"

    name: Mapped[str] = mapped_column(String(length=100), nullable=False)
    website: Mapped[str] = mapped_column(String(length=200), index=True)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))

    user: Mapped["User"] = relationship(back_populates="merchant")
    api_keys: Mapped[list["APIKey"]] = relationship(back_populates="merchant")
    integrations: Mapped[list["Integration"]] = relationship(back_populates="merchant", cascade="all, delete-orphan")
    invoices: Mapped[list["Invoice"]] = relationship(back_populates="merchant", cascade="all, delete-orphan")
    payments: Mapped[list["Payment"]] = relationship(back_populates="merchant", cascade="all, delete-orphan")
    payouts: Mapped[list["Payout"]] = relationship(back_populates="merchant", cascade="all, delete-orphan")
    webhooks: Mapped[list["Webhook"]] = relationship(back_populates="merchant", cascade="all, delete-orphan")
