from decimal import Decimal
from enum import StrEnum
from uuid import UUID

from sqlalchemy import Enum, ForeignKey, Integer, Numeric, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class BlockchainTransactionStatus(StrEnum):
    PENDING = "pending"
    CONFIRMED = "confirmed"
    FAILED = "failed"
    DROPPED = "dropped"
    REPLACED = "replaced"


class BlockchainTransaction(AbstractBase):
    __tablename__ = "blockchain_transactions"

    amount: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8), nullable=False)
    status: Mapped[BlockchainTransactionStatus] = mapped_column(
        Enum(BlockchainTransactionStatus),
        nullable=False,
        index=True,
    )
    transaction_hash: Mapped[str] = mapped_column(String, nullable=False, index=True)
    block_number: Mapped[int | None] = mapped_column(Integer, nullable=True)
    from_address: Mapped[str] = mapped_column(String, nullable=False)
    to_address: Mapped[str] = mapped_column(String, nullable=False)
    currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))

    currency: Mapped["Currency"] = relationship(back_populates="blockchain_transactions")
    payment: Mapped["Payment | None"] = relationship(back_populates="blockchain_transaction")
    payout: Mapped["Payout | None"] = relationship(back_populates="blockchain_transaction")
