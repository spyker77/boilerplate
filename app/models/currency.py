from sqlalchemy import Boolean, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase
from app.models.exchange_rate import ExchangeRate


class Currency(AbstractBase):
    __tablename__ = "currencies"

    code: Mapped[str] = mapped_column(String(length=10), unique=True, nullable=False, index=True)
    name: Mapped[str] = mapped_column(String(length=50), nullable=False, index=True)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)

    blockchain_transactions: Mapped[list["BlockchainTransaction"]] = relationship(back_populates="currency")
    invoices: Mapped[list["Invoice"]] = relationship(back_populates="currency")
    payments: Mapped[list["Payment"]] = relationship(back_populates="currency")
    payouts: Mapped[list["Payout"]] = relationship(back_populates="currency")
    wallets: Mapped[list["Wallet"]] = relationship(back_populates="currency")

    base_rates: Mapped[list["ExchangeRate"]] = relationship(
        back_populates="base_currency",
        foreign_keys=[ExchangeRate.base_currency_id],
    )
    quote_rates: Mapped[list["ExchangeRate"]] = relationship(
        back_populates="quote_currency",
        foreign_keys=[ExchangeRate.quote_currency_id],
    )
