from datetime import datetime
from decimal import Decimal
from enum import StrEnum
from uuid import UUID

from sqlalchemy import DateTime, Enum, ForeignKey, Numeric, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class PayoutStatus(StrEnum):
    PENDING = "pending"
    PROCESSING = "processing"
    COMPLETED = "completed"
    FAILED = "failed"
    CANCELLED = "cancelled"


class Payout(AbstractBase):
    __tablename__ = "payouts"

    amount: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8), nullable=False)
    status: Mapped[PayoutStatus] = mapped_column(Enum(PayoutStatus), nullable=False, index=True)
    address: Mapped[str] = mapped_column(String)
    transaction_hash: Mapped[str | None] = mapped_column(String, nullable=True)
    completed_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))
    blockchain_transaction_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("blockchain_transactions.id"),
        nullable=True,
    )
    currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))
    merchant_id: Mapped[UUID] = mapped_column(ForeignKey("merchants.id"))

    user: Mapped["User"] = relationship(back_populates="payouts")
    blockchain_transaction: Mapped["BlockchainTransaction | None"] = relationship(back_populates="payout")
    currency: Mapped["Currency"] = relationship(back_populates="payouts")
    merchant: Mapped["Merchant"] = relationship(back_populates="payouts")
