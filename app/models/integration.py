from enum import StrEnum
from uuid import UUID

from sqlalchemy import JSON, Boolean, Enum, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class IntegrationType(StrEnum):
    PAYMENT_GATEWAY = "payment_gateway"
    EXCHANGE = "exchange"
    ANALYTICS = "analytics"
    ACCOUNTING = "accounting"
    CRM = "crm"


class Integration(AbstractBase):
    __tablename__ = "integrations"

    integration_type: Mapped[IntegrationType] = mapped_column(Enum(IntegrationType), nullable=False, index=True)
    config: Mapped[dict] = mapped_column(JSON, nullable=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    merchant_id: Mapped[UUID] = mapped_column(ForeignKey("merchants.id"))
    partner_id: Mapped[UUID] = mapped_column(ForeignKey("partners.id"))

    merchant: Mapped["Merchant"] = relationship(back_populates="integrations")
    partner: Mapped["Partner"] = relationship(back_populates="integrations")
