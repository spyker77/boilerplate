from datetime import datetime
from decimal import Decimal
from enum import StrEnum
from uuid import UUID

from sqlalchemy import DateTime, Enum, ForeignKey, Numeric, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class PaymentStatus(StrEnum):
    PENDING = "pending"
    PROCESSING = "processing"
    COMPLETED = "completed"
    FAILED = "failed"
    REFUNDED = "refunded"


class Payment(AbstractBase):
    __tablename__ = "payments"

    amount: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8), nullable=False)
    status: Mapped[PaymentStatus] = mapped_column(Enum(PaymentStatus), nullable=False, index=True)
    transaction_hash: Mapped[str | None] = mapped_column(String, nullable=True)
    completed_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))
    blockchain_transaction_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("blockchain_transactions.id"),
        nullable=True,
    )
    currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))
    invoice_id: Mapped[UUID] = mapped_column(ForeignKey("invoices.id"))
    merchant_id: Mapped[UUID] = mapped_column(ForeignKey("merchants.id"))

    user: Mapped["User"] = relationship(back_populates="payments")
    blockchain_transaction: Mapped["BlockchainTransaction | None"] = relationship(back_populates="payment")
    currency: Mapped["Currency"] = relationship(back_populates="payments")
    invoice: Mapped["Invoice"] = relationship(back_populates="payments")
    merchant: Mapped["Merchant"] = relationship(back_populates="payments")
