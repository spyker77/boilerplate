from decimal import Decimal
from uuid import UUID

from sqlalchemy import ForeignKey, Numeric, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class Wallet(AbstractBase):
    __tablename__ = "wallets"

    balance: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8), default=0)
    address: Mapped[str] = mapped_column(String)
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))
    currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))

    user: Mapped["User"] = relationship(back_populates="wallets")
    currency: Mapped["Currency"] = relationship(back_populates="wallets")
