from datetime import datetime
from decimal import Decimal
from enum import StrEnum
from uuid import UUID

from sqlalchemy import DateTime, Enum, ForeignKey, Numeric, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models._base import AbstractBase


class InvoiceStatus(StrEnum):
    PENDING = "pending"
    PAID = "paid"
    EXPIRED = "expired"
    CANCELLED = "cancelled"


class Invoice(AbstractBase):
    __tablename__ = "invoices"

    amount: Mapped[Decimal] = mapped_column(Numeric(precision=18, scale=8))
    status: Mapped[InvoiceStatus] = mapped_column(Enum(InvoiceStatus), nullable=False, index=True)
    description: Mapped[str | None] = mapped_column(String, nullable=True)
    expires_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    paid_at: Mapped[datetime | None] = mapped_column(DateTime(timezone=True), nullable=True)
    user_id: Mapped[UUID] = mapped_column(ForeignKey("users.id"))
    merchant_id: Mapped[UUID] = mapped_column(ForeignKey("merchants.id"))
    currency_id: Mapped[UUID] = mapped_column(ForeignKey("currencies.id"))

    user: Mapped["User"] = relationship(back_populates="invoices")
    currency: Mapped["Currency"] = relationship(back_populates="invoices")
    merchant: Mapped["Merchant"] = relationship(back_populates="invoices")
    payments: Mapped[list["Payment"]] = relationship(back_populates="invoice", cascade="all, delete-orphan")
