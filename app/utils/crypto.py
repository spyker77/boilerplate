from collections.abc import Sequence
from decimal import Decimal

import httpx
from loguru import logger
from tenacity import retry, stop_after_attempt, wait_random_exponential

from app.config import MISC_CONF
from app.models.currency import Currency


@retry(stop=stop_after_attempt(3), wait=wait_random_exponential(min=0.1, max=1))
async def fetch_binance_ticker_price() -> list[dict[str, str]]:
    async with httpx.AsyncClient() as client:
        response = await client.get(MISC_CONF.BINANCE_API_URL + "/api/v3/ticker/price")
        response.raise_for_status()
        return response.json()


async def get_exchange_rates(currencies: Sequence[Currency]) -> dict[str, dict[str, Decimal]]:
    try:
        data = await fetch_binance_ticker_price()
    except httpx.HTTPStatusError as e:
        logger.error(f"HTTP error occurred: {e}")
        return {}
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        return {}

    # Create a set of currency codes for faster lookup.
    currency_codes = {currency.code for currency in currencies}
    rates = {code: {} for code in currency_codes}

    for item in data:
        symbol = item["symbol"]
        price = Decimal(item["price"])

        # Check if the symbol starts with any of our currency codes.
        for base in currency_codes:
            if symbol.startswith(base):
                quote = symbol[len(base) :]

                # If both base and quote are in our currency codes, add the rate.
                if quote in currency_codes:
                    rates[base][quote] = price
                    rates[quote][base] = Decimal("1") / price  # also add the inverse rate.

    # Add identity rates 1:1 for same currency.
    for code in currency_codes:
        rates[code][code] = Decimal("1")

    return rates
