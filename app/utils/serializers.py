from functools import singledispatch
from typing import Any
from uuid import UUID

import orjson
from pydantic import BaseModel


def key_serializer(key: str | None) -> bytes | None:
    return key.encode("utf-8") if key else None


def key_deserializer(key: bytes | None) -> str | None:
    return key.decode("utf-8") if key else None


@singledispatch
def json_serializer(obj: Any) -> bytes:
    return orjson.dumps(obj, default=_custom_json_encoder)


@json_serializer.register(bytes)
def _(obj: bytes) -> bytes:
    return obj


@json_serializer.register(BaseModel)
def _(obj: BaseModel) -> bytes:
    return obj.model_dump_json().encode("utf-8")


def json_deserializer(obj: bytes | None) -> Any:
    if not obj or not obj.strip():
        return None
    return orjson.loads(obj)


def _custom_json_encoder(obj) -> str:
    if isinstance(obj, UUID):
        return str(obj)
    raise TypeError
