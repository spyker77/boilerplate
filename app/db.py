from collections.abc import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy.pool import NullPool  # use pgbouncer for a connection pooling

from app.config import DB_URL

engine = create_async_engine(DB_URL, pool_pre_ping=True, poolclass=NullPool)
async_session_maker = async_sessionmaker(engine, autoflush=False, expire_on_commit=False)


async def get_db() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        try:
            yield session
        except Exception:
            await session.rollback()
            raise
