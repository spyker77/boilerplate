import taskiq_fastapi
from taskiq import TaskiqScheduler
from taskiq.schedule_sources import LabelScheduleSource
from taskiq_aio_pika import AioPikaBroker

from app.config import MISC_CONF, RABBIT_CONF


class CustomLabelScheduleSource(LabelScheduleSource):
    async def get_schedules(self) -> list:
        return await super().get_schedules() if MISC_CONF.TASKIQ_SCHEDULER_ENABLED else []


broker = AioPikaBroker(RABBIT_CONF.URL)
scheduler = TaskiqScheduler(broker=broker, sources=[CustomLabelScheduleSource(broker)])

taskiq_fastapi.init(broker, "app.main:app")
