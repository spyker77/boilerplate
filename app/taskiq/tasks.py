from datetime import UTC, datetime, timedelta

from loguru import logger
from sqlalchemy import select
from taskiq import TaskiqDepends

from app.db import get_db
from app.models.api_key import APIKey
from app.models.currency import Currency
from app.models.exchange_rate import ExchangeRate
from app.redis import get_redis
from app.services.api_key import APIKeyService
from app.taskiq import broker
from app.utils.crypto import get_exchange_rates


@broker.task(schedule=[{"cron": "* * * * *"}])
async def update_exchange_rates(db=TaskiqDepends(get_db)):
    currencies = await db.execute(select(Currency).where(Currency.is_active))
    active_currencies = currencies.scalars().all()

    rates = await get_exchange_rates(active_currencies)

    for base, quotes in rates.items():
        base_currency = await db.execute(select(Currency).where(Currency.code == base))
        base_currency = base_currency.scalar_one()

        for quote, rate in quotes.items():
            quote_currency = await db.execute(select(Currency).where(Currency.code == quote))
            quote_currency = quote_currency.scalar_one()

            result = await db.execute(
                select(ExchangeRate).where(
                    ExchangeRate.base_currency_id == base_currency.id,
                    ExchangeRate.quote_currency_id == quote_currency.id,
                )
            )

            if exchange_rate := result.scalar_one_or_none():
                exchange_rate.rate = rate
                exchange_rate.updated_at = datetime.now(UTC)
            else:
                new_rate = ExchangeRate(
                    base_currency_id=base_currency.id,
                    quote_currency_id=quote_currency.id,
                    rate=rate,
                )
                db.add(new_rate)

    await db.commit()
    logger.info("Updated exchange rates")


@broker.task(schedule=[{"cron": "0 0 * * *"}])
async def rotate_old_api_keys(db=TaskiqDepends(get_db), redis=TaskiqDepends(get_redis)):
    api_key_service = APIKeyService(db, redis)

    thirty_days_ago = datetime.now(UTC) - timedelta(days=30)
    old_keys = await db.execute(select(APIKey).where(APIKey.rotated_at < thirty_days_ago))
    for api_key in old_keys.scalars():
        await api_key_service.rotate_api_key(api_key.key)
