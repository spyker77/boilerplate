from collections.abc import AsyncGenerator

from redis.asyncio import Redis

from app.config import REDIS_CONF


async def get_redis() -> AsyncGenerator[Redis, None]:
    async with Redis(host=REDIS_CONF.HOST, port=REDIS_CONF.PORT, decode_responses=True) as client:
        try:
            yield client
        finally:
            await client.aclose()
