from datetime import UTC, datetime, timedelta
from enum import StrEnum
from typing import Any
from uuid import uuid4

import jwt
from pydantic import BaseModel
from redis.asyncio import Redis

from app.config import MISC_CONF

ALGORITHM = "HS256"
TOKEN_PREFIX = "token:"


class JSONWebToken(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class TokenType(StrEnum):
    ACCESS = "access"
    REFRESH = "refresh"
    VERIFY = "verify"
    RESET = "reset"
    BEARER = "bearer"


async def create_token(
    data: dict[str, Any],
    token_type: TokenType,
    expires_delta: timedelta | None = None,
    audience: str = "boilerplate",
    redis: Redis | None = None,
) -> str:
    payload = data.copy()

    if expires_delta:
        expires_at = datetime.now(UTC) + expires_delta
    else:
        expires_at = datetime.now(UTC) + (
            timedelta(minutes=MISC_CONF.ACCESS_TOKEN_EXPIRE_MINUTES)
            if token_type != TokenType.REFRESH
            else timedelta(days=MISC_CONF.REFRESH_TOKEN_EXPIRE_DAYS)
        )

    jti = str(uuid4())
    payload.update(
        {
            "exp": expires_at,
            "iat": datetime.now(UTC),
            "jti": jti,
            "aud": audience,
            "token_type": token_type.value,
        }
    )
    token = jwt.encode(payload, MISC_CONF.SECRET_KEY, algorithm=ALGORITHM)

    if redis and token_type in [TokenType.ACCESS, TokenType.REFRESH]:
        cache_expiry = int((expires_at - datetime.now(UTC)).total_seconds())
        await redis.setex(f"{TOKEN_PREFIX}{jti}", cache_expiry, "valid")

    return token


async def verify_token(
    token: str,
    expected_type: TokenType,
    expected_audience: str = "boilerplate",
    redis: Redis | None = None,
) -> str | None:
    try:
        payload = jwt.decode(token, MISC_CONF.SECRET_KEY, algorithms=[ALGORITHM], audience=expected_audience)
    except jwt.PyJWTError:
        return None

    email = payload.get("sub")
    token_type = payload.get("token_type")
    expiration = payload.get("exp")
    jti = payload.get("jti")

    if (
        not email
        or (token_type != expected_type.value)
        or (datetime.fromtimestamp(expiration, UTC) < datetime.now(UTC))
    ):
        return None

    if redis and TokenType(token_type) in [TokenType.ACCESS, TokenType.REFRESH]:
        token_status = await redis.get(f"{TOKEN_PREFIX}{jti}")
        if token_status != "valid":
            return None

    return email


async def decode_token(
    token: str,
    expected_audience: str = "boilerplate",
    redis: Redis | None = None,
) -> dict[str, Any] | None:
    try:
        payload = jwt.decode(token, MISC_CONF.SECRET_KEY, algorithms=[ALGORITHM], audience=expected_audience)
    except jwt.PyJWTError:
        return None

    token_type = payload.get("token_type")
    jti = payload.get("jti")

    if redis and TokenType(token_type) in [TokenType.ACCESS, TokenType.REFRESH]:
        token_status = await redis.get(f"{TOKEN_PREFIX}{jti}")
        if token_status != "valid":
            return None

    return payload


async def revoke_token(jti: str, redis: Redis):
    await redis.delete(f"{TOKEN_PREFIX}{jti}")


async def is_token_revoked(jti: str, redis: Redis) -> bool:
    return await redis.get(f"{TOKEN_PREFIX}{jti}") is None
