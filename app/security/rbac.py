from enum import StrEnum
from functools import cache


class Permission(StrEnum):
    VIEW_DASHBOARD = "view_dashboard"
    VIEW_INVOICE = "view_invoice"
    VIEW_PAYOUT = "view_payout"
    VIEW_REPORTS = "view_reports"
    VIEW_USER_STATISTICS = "view_user_statistics"
    VIEW_MERCHANT_STATISTICS = "view_merchant_statistics"
    VIEW_OVERALL_STATISTICS = "view_overall_statistics"

    CREATE_INVOICE = "create_invoice"
    CREATE_PAYOUT = "create_payout"

    CANCEL_INVOICE = "cancel_invoice"
    CANCEL_PAYOUT = "cancel_payout"

    MANAGE_USERS = "manage_users"
    MANAGE_API_KEYS = "manage_api_keys"
    MANAGE_SETTINGS = "manage_settings"


class Role(StrEnum):
    USER = "user"
    MERCHANT = "merchant"
    SUPPORT = "support"
    ADMIN = "admin"

    @classmethod
    @property
    @cache
    def hierarchy(cls) -> list["Role"]:
        return [cls.USER, cls.MERCHANT, cls.SUPPORT, cls.ADMIN]

    @classmethod
    def is_sufficient(cls, user_role: "Role", required_role: "Role") -> bool:
        return cls.hierarchy.index(user_role) >= cls.hierarchy.index(required_role)


BASE_PERMISSIONS: frozenset[Permission] = frozenset(
    {
        Permission.VIEW_DASHBOARD,
        Permission.VIEW_INVOICE,
        Permission.VIEW_PAYOUT,
        Permission.VIEW_USER_STATISTICS,
    }
)

MERCHANT_PERMISSIONS: frozenset[Permission] = BASE_PERMISSIONS | frozenset(
    {
        Permission.VIEW_MERCHANT_STATISTICS,
        Permission.CREATE_INVOICE,
        Permission.CREATE_PAYOUT,
        Permission.MANAGE_API_KEYS,
    }
)

SUPPORT_PERMISSIONS: frozenset[Permission] = MERCHANT_PERMISSIONS | frozenset(
    {
        Permission.VIEW_REPORTS,
        Permission.CANCEL_INVOICE,
        Permission.CANCEL_PAYOUT,
    }
)

ROLE_PERMISSIONS: dict[Role, frozenset[Permission]] = {
    Role.USER: BASE_PERMISSIONS,
    Role.MERCHANT: MERCHANT_PERMISSIONS,
    Role.SUPPORT: SUPPORT_PERMISSIONS,
    Role.ADMIN: frozenset(Permission),
}
