from collections.abc import Sequence

from fastapi import HTTPException, status

from app.security.rbac import ROLE_PERMISSIONS, Permission, Role


class PolicyEnforcer:
    """A class to enforce role-based access control policies.

    This class checks if a user has the required role and permissions
    to access a particular resource or perform a specific action.
    It uses the ROLE_PERMISSIONS mapping to determine user permissions.
    """

    async def enforce(
        self,
        user_role: Role,
        required_role: Role | None = None,
        required_permissions: Sequence[Permission] | None = None,
    ) -> None:
        """
        Enforce the access control policy for a given user.

        This method checks if the user has the required role and/or permissions.
        If both role and permissions are specified, the user must meet both criteria.
        If the user doesn't meet the requirements, it raises an HTTPException.

        Args:
            user_role (Role): The role of the user to check.
            required_role (Role | None): The minimum role required for access. Defaults to None.
            required_permissions (Sequence[Permission] | None): The permissions required for access. Defaults to None.

        Raises:
            HTTPException: If the user doesn't have the required role or permissions.
        """
        if required_role:
            if not Role.is_sufficient(user_role, required_role):
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient role")

        if required_permissions:
            user_permissions = ROLE_PERMISSIONS[user_role]
            if not all(perm in user_permissions for perm in required_permissions):
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions")


def get_policy_enforcer():
    return PolicyEnforcer()
