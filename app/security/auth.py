from fastapi import Depends, HTTPException, status
from fastapi.security import APIKeyHeader, OAuth2PasswordBearer
from redis.asyncio import Redis

from app.models.user import User
from app.redis import get_redis
from app.security.jwt import JSONWebToken, TokenType, create_token, decode_token, revoke_token, verify_token
from app.services.api_key import APIKeyService, get_api_key_service
from app.services.user import UserService, get_user_service

WWW_AUTHENTICATE_HEADER = {"WWW-Authenticate": "Bearer"}

api_key_header = APIKeyHeader(name="X-API-Key", auto_error=False)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/v1/auth/login")  # a common prefix and path to the login method


async def authenticate_user(email: str, password: str, user_service: UserService) -> User:
    user = await user_service.get_user_by_email(email)
    if user and await user_service.verify_and_update_password(user, password):
        return user
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers=WWW_AUTHENTICATE_HEADER,
        )


async def create_auth_tokens(user: User, redis: Redis) -> JSONWebToken:
    access_token = await create_token(data={"sub": user.email}, token_type=TokenType.ACCESS, redis=redis)
    refresh_token = await create_token(data={"sub": user.email}, token_type=TokenType.REFRESH, redis=redis)
    return JSONWebToken(access_token=access_token, refresh_token=refresh_token, token_type=TokenType.BEARER)


async def refresh_auth_tokens(refresh_token: str, user_service: UserService, redis: Redis) -> JSONWebToken:
    email = await verify_token(refresh_token, expected_type=TokenType.REFRESH, redis=redis)
    if not email:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid or expired token",
            headers=WWW_AUTHENTICATE_HEADER,
        )

    user = await user_service.get_user_by_email(email)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User not found",
            headers=WWW_AUTHENTICATE_HEADER,
        )

    payload = await decode_token(token=refresh_token, redis=redis)
    if payload and (jti := payload.get("jti")):
        await revoke_token(jti, redis)

    return await create_auth_tokens(user, redis)


async def get_user_from_token(token: str, user_service: UserService, redis: Redis) -> User:
    email = await verify_token(token, expected_type=TokenType.ACCESS, redis=redis)
    if not email:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid or expired token",
            headers=WWW_AUTHENTICATE_HEADER,
        )

    user = await user_service.get_user_by_email(email)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User not found",
            headers=WWW_AUTHENTICATE_HEADER,
        )

    return user


async def get_user_from_api_key(key: str, api_key_service: APIKeyService, user_service: UserService) -> User:
    api_key = await api_key_service.get_api_key(key)
    if not api_key:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API key")

    user = await user_service.get_user_by_id(api_key.user_id)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="User not found")

    return user


async def get_current_user(
    api_key: str | None = Depends(api_key_header),
    token: str = Depends(oauth2_scheme),
    user_service: UserService = Depends(get_user_service),
    api_key_service: APIKeyService = Depends(get_api_key_service),
    redis: Redis = Depends(get_redis),
) -> User:
    if api_key:
        return await get_user_from_api_key(api_key, api_key_service, user_service)

    return await get_user_from_token(token, user_service, redis)
