import base64
import io
import secrets

import pyotp
import qrcode
from pydantic import BaseModel

from app.models.user import User
from app.schemas.user import UserUpdate
from app.security.password import get_password_hash
from app.services.user import UserService


class TwoFactorSetup(BaseModel):
    secret: str
    qr_code: str  # Base64 encoded string

    @classmethod
    def from_qr_code(cls, secret: str, qr_code: bytes):
        return cls(secret=secret, qr_code=base64.b64encode(qr_code).decode("utf-8"))


async def generate_2fa_secret(user: User, user_service: UserService) -> TwoFactorSetup:
    secret = pyotp.random_base32()
    data = UserUpdate(two_factor_enabled=True, two_factor_secret=secret)
    await user_service.update_user(user.id, data)

    totp = pyotp.TOTP(secret)
    provisioning_uri = totp.provisioning_uri(user.email, issuer_name="boilerplate")
    image = qrcode.make(provisioning_uri)
    image_byte_array = io.BytesIO()
    image.save(image_byte_array, format="PNG")
    qr_code = image_byte_array.getvalue()

    return TwoFactorSetup.from_qr_code(secret=secret, qr_code=qr_code)


def verify_2fa_code(user: User, code: str) -> bool:
    if not user or not user.two_factor_enabled or not user.two_factor_secret:
        return False

    totp = pyotp.TOTP(user.two_factor_secret)
    return totp.verify(code)


async def generate_backup_codes(user: User, user_service: UserService) -> list[str]:
    backup_codes = [secrets.token_hex(4) for _ in range(10)]  # generate 10 backup codes
    hashed_backup_codes = [get_password_hash(code) for code in backup_codes]
    await user_service.update_user(user.id, UserUpdate(backup_codes=hashed_backup_codes))
    return backup_codes
