# Boilerplate

A scalable, robust, and production-ready system - with user management - that serves as a foundation for developing high-performance, high-load web applications and APIs.

Built on top of FastAPI, SQLAlchemy, and TaskIQ, this implementation fully embraces asyncio and features advanced database connection pooling with PgBouncer, distributed task processing, CI/CD pipelines, Kubernetes-ready deployment for different environments using Helm charts and more.

Ideal for small to medium-sized teams aiming to rapidly develop and scale modern web services.

## Table of Contents

1. [Getting Started](#getting-started)
   - [Prerequisites](#prerequisites)
   - [Development Setup](#development-setup)
   - [Running Tests](#running-tests)
   - [Running with Helm](./helm/README.md)
2. [Development and Deployment](#development-and-deployment)
   - [Feature Development](#feature-development)
   - [Production Release](#production-release)
   - [Rollback Strategy](#rollback-strategy)
3. [Design Decisions](#design-decisions)
   - [Modular Monolith](#modular-monolith)
   - [Role-Based Access Control (RBAC)](#role-based-access-control-rbac)
   - [Database Connection Management](#database-connection-management)
   - [Asynchronous Task Queue](#asynchronous-task-queue)
   - [Multi-Platform Builds](#multi-platform-builds)
   - [Future Considerations](#future-considerations)

## Getting Started

### Prerequisites

- Python 3.12+
- Poetry
- Docker and Docker Compose
- Kubernetes
- Helm

### Development Setup

1. Install dependencies:

   ```bash
   poetry install --no-root
   ```

2. Install pre-commit hooks inside activated venv:

   ```bash
   poetry shell
   pre-commit install
   ```

3. Set up environment variables by copying `.env.example` to `.env` and fill in the necessary values.

4. Run the application with its services:

   ```bash
   docker compose up --build
   ```

5. Access the Swagger UI at <http://localhost:8000/docs>.

### Running Tests

To simplify test runs, use commands from the [Makefile](./Makefile):

   ```bash
   make run-tests
   ```

To run locust for a performance testing in activated venv and with running application:

   ```bash
   make run-locust
   ```

Alternatively, you can try benchmarking with [ApacheBench](https://httpd.apache.org/docs/current/programs/ab.html), and here's an example for the application running on K8s with 1 replica:

   ```bash
   $ ab -n 10000 -c 100 http://127.0.0.1.nip.io/api/v1/users/me
   This is ApacheBench, Version 2.3 <$Revision: 1903618 $>
   Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
   Licensed to The Apache Software Foundation, http://www.apache.org/

   Benchmarking 127.0.0.1.nip.io (be patient)
   Completed 1000 requests
   Completed 2000 requests
   Completed 3000 requests
   Completed 4000 requests
   Completed 5000 requests
   Completed 6000 requests
   Completed 7000 requests
   Completed 8000 requests
   Completed 9000 requests
   Completed 10000 requests
   Finished 10000 requests


   Server Software:        uvicorn
   Server Hostname:        127.0.0.1.nip.io
   Server Port:            80

   Document Path:          /api/v1/users/me
   Document Length:        30 bytes

   Concurrency Level:      100
   Time taken for tests:   7.215 seconds
   Complete requests:      10000
   Failed requests:        0
   Non-2xx responses:      10000
   Total transferred:      1910000 bytes
   HTML transferred:       300000 bytes
   Requests per second:    1386.09 [#/sec] (mean)
   Time per request:       72.146 [ms] (mean)
   Time per request:       0.721 [ms] (mean, across all concurrent requests)
   Transfer rate:          258.54 [Kbytes/sec] received

   Connection Times (ms)
               min  mean[+/-sd] median   max
   Connect:        0    1   0.6      1      11
   Processing:     5   71  82.5     68    1880
   Waiting:        5   70  82.5     67    1880
   Total:          5   72  82.7     69    1881

   Percentage of the requests served within a certain time (ms)
   50%     69
   66%     69
   75%     70
   80%     70
   90%     72
   95%     94
   98%     97
   99%     98
   100%   1881 (longest request)
   ```

## Development and Deployment

We use GitHub flow with two environments: development (dev) and production (prod).

### Feature Development

1. **Make Changes**
   - Create a new branch from `main`.
   - Develop and test locally.
   - Push your changes and open merge request to merge into `main`.

2. **Continuous Integration**
   - Automated linters, migration checks, and tests run on all merge requests and pushes to `main`.

3. **Merging to Main**
   - Once approved, merge request is merged into `main`.
   - CI/CD automatically builds and deploys to the dev environment using the tag `latest`.

4. **Dev Testing**
   - Perform additional testing in the dev environment.

### Production Release

1. **Prepare Release**
   - Ensure all changes for the release are merged into the `main` branch.
   - Determine the new version number following [semantic versioning](https://semver.org/) (e.g., v1.2.3).

2. **Tag and Release**
   - Create and push a new Git tag for the release:

   ```bash
   git checkout main
   git pull
   git tag v1.2.3
   git push origin v1.2.3
   ```

   - This action triggers the production pipeline.

3. **Build and Deploy**

   - Production pipeline automatically:
     - Builds a new Docker image tagged with the version number.
     - Pushes the image to the registry.
     - Deploys the new version to the production environment.

### Rollback Strategy

In case of issues in production, we can quickly rollback to a previous version:

1. Identify the last stable version (e.g., v1.2.2).
2. Trigger deployment of this version trough a CI/CD pipeline:

   ```bash
   git checkout main
   git push origin v1.2.2
   ```

⚠️ NOTE: This approach works well for a simple rollbacks that don't involve database schema changes. For cases involving database changes, refer to [README.md](./alembic/README.md) inside the alembic folder.

## Design Decisions

 In developing this system, several key architectural decisions were made and patterns considered to balance simplicity, maintainability, and scalability. Here's an overview of the approach and reasoning behind it.

### Modular Monolith

We've adopted a modular monolith architecture ([ref. Google's paper](https://dl.acm.org/doi/pdf/10.1145/3593856.3595909)) with a service-layer pattern, that offers the following key advantages:

1. **Modular Structure**: The application is divided into distinct modules, improving code organization and maintainability while being deployed as a single unit. This approach provides better separation of concerns compared to a traditional monolith, while avoiding the complexity of microservices.

2. **Service Layer Pattern**: We use a service layer to encapsulate business logic, separating it from API endpoints and data models. This approach provides clear boundaries between application layers without requiring extensive experience with more complex architectural patterns.

3. **Locality of Behavior**: Our architecture promotes the principle of keeping behavior close to the data it operates on. This principle enhances code readability and maintainability, making it easier for developers to understand and modify features, which is crucial in the fast-paced environment.

4. **Balanced Complexity**: By reducing the number of abstraction layers and finding a middle ground between simpler patterns like MVC and more complex ones like hexagonal architecture, we incorporate beneficial aspects of various patterns without overengineering.

5. **Scalability Path**: While currently a monolith, our modular design facilitates a potential future transition to microservices or event-driven architecture if needed, as outlined in the [future considerations](#future-considerations).

### Role-Based Access Control (RBAC)

We've implemented a custom PolicyEnforcer for handling role-based access control. This approach offers several advantages:

- **Efficiency**: Our PolicyEnforcer provides a high-performance solution for role and permission checks. By using predefined role-permission mappings and caching the role hierarchy, we avoid additional database queries and ensure quick access control decisions.

- **Flexibility**: The PolicyEnforcer allows for a granular control over roles and permissions. It supports checking for a specific roles, individual permissions, or combinations of both, providing flexibility to handle complex access control scenarios without compromising on performance.

### Database Connection Management

We've added PgBouncer for database connection pooling and disabled SQLAlchemy's built-in pooling for the following benefits:

1. **Improved Scalability**: PgBouncer allows us to efficiently manage database connections across multiple replicas during horizontal autoscaling of our application in Kubernetes.

2. **Optimized Resource Utilization**: Centralizing pooling with PgBouncer avoids conflicts between application-level (SQLAlchemy) and infrastructure-level connection management.

3. **Session Pool Mode**: We use PgBouncer's session pool mode to maintain compatibility with asyncpg, particularly for prepared statements, and prevent issues with statement caching that occurs in transaction pool mode.

### Asynchronous Task Queue

We chose [TaskIQ](https://taskiq-python.github.io/) over alternatives like [Celery](https://docs.celeryq.dev/) or [Dramatiq](https://dramatiq.io/) for our distributed task processing because:

1. **Full Asyncio Integration**: TaskIQ's native asyncio design integrates seamlessly with our FastAPI-based application, leading to a more efficient handling of I/O-bound tasks and better overall performance.

2. **Resource Optimization**: Our current TaskIQ setup requires about half as many CPU and memory compared to the previous Celery setup, potentially reducing infrastructure costs.

3. **Enhanced Developer Experience**: TaskIQ leverages modern Python features like strong typing as well as FastAPI's dependency injection system, plus allows to avoid tricks like [nest-asyncio](https://pypi.org/project/nest-asyncio/) for sync code.

### Multi-Platform Builds

The project also builds a multi-platform Docker images for both `linux/amd64` and `linux/arm64` architectures using Docker BuildKit in the CI/CD pipeline. This approach has several pros:

- Enhanced development flexibility across different machine types.
- Potential for performance and cost optimization.
- Flexibility in cloud deployment options.

### Future Considerations

As our project grows, we can evolve towards a more complex architecture combining API-driven and event-driven microservices. This hybrid approach allows for flexibility in handling different types of operations and interactions. Here's how we can evolve:

1. Enhance current architecture:
   - Introduce repositories for complex data access patterns.
   - Implement more rigorous domain modeling within our services.

2. Gradual transition to microservices:
   - Start by extracting the most independent or critical services.
   - Implement an API gateway to manage and route external requests.

3. Develop two types of microservices:

   a. API-driven microservices:
      - Directly accessible through the API gateway.
      - Handle synchronous operations and immediate responses.
      - Examples: user authentication, real-time data queries.

   b. Event-driven microservices:
      - Operate based on asynchronous events.
      - Not directly accessible via API; communicate through an event bus.
      - Handle background processes, data updates, and complex workflows.
      - Examples: payment processing, analytics, notification services.

4. Implement event-driven architecture components:
   - Set up an event bus or message broker (e.g., Strimzi Kafka).
   - Design services to publish and subscribe to domain events.
   - Implement event sourcing and CQRS patterns where appropriate.

5. Adapt the API gateway:
   - Handle routing for API-driven microservices.
   - Integrate with the event bus to trigger events from API calls when necessary.

By starting with this simplified yet structured approach, we maintain the agility to rapidly develop features while keeping the door open for future architectural changes, and avoid wasting resources on premature optimization.
